#ifndef CLIENTE_H_INCLUDED
#define CLIENTE_H_INCLUDED

#include <string>
#include <vector>

#include "utilidades.h"

using namespace std;


class Cliente {
	int id;
    static int ultimoId;
    string nome;
	string contacto;
public:
   Cliente(int tempId);
   Cliente(string tempNome, string tempContacto = "");
   Cliente();
   Cliente(int tempId, string tempNome, string tempContacto = "");
   /** @brief Retorna o Id de um Cliente.@return int com o Id do Cliente. **/
   int getId() const { return id; }
   /** @brief Retorna o Nome de um Cliente. @return string com o Nome do Cliente **/
   string getNome() const { return nome;}
   /** @brief Retorna o contacto de um Cliente. @return string com o Contacto do Cliente **/
   string getContacto() const { return contacto; }
   /** @brief Altera o nome do Produtor com um novo nome Fornecido. @param Novo Nome **/
   void setNome(string Nome) { nome=Nome; }
   /** @brief Altera o contacto do Cliente com um novo contacto fornecido. @param Novo Contacto **/
   void setContacto(string Contacto) { contacto=Contacto; }

   /**@brief Retorna um ostream para que os dados sejam escritos na consola @param ostream e o cliente a escrever @return ostream com os dados **/ 
   friend ostream & operator<<(ostream &o, const Cliente &c);

};


#endif // CLIENTE_H_INCLUDED
