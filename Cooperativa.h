﻿#ifndef COOPERATIVA_H_INCLUDED
#define COOPERATIVA_H_INCLUDED

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cctype>

#include <unordered_set>

#include "produto.h"
#include "produtor.h"
#include "cliente.h"
#include "transacao.h"
#include "utilidades.h"
#include "BST.h"
#include "empresatransporte.h"


using namespace std;

struct hCliente {
	int operator() (const Cliente &cli1) const
	{
		return 1/cli1.getId();
	}

	bool operator() (const Cliente &cli1, const Cliente &cli2) const
	{
		return (cli1.getId() == cli2.getId());
	}
};

typedef unordered_set<Cliente, hCliente, hCliente> hashCliente;

class Cooperativa {
	string nome;
	vector<Produtor *> produtores;
	vector<Cliente *> clientes;
	vector<Transacao *> transacoes;
	BST<ProdutoPtr> produtos; //Ordenada pelo nome (a.k.a. designação do produto)
	vector<Uva *> uvas;
	hashCliente clientesinativos;
	priority_queue< EmpresaTransporte > empresasTransporte;

	/** @brief Auxilia a leitura do ficheiro @param linha lida @return estado a usar **/
	int converteEmEstado(string linha) const;
	/** @brief Interpreta cada linha do ficheiro consoante um estado @param linda lida e estado respectido **/
	void leLinha(string linha, int estado);
public:
	Cooperativa(string tempNome);
	Cooperativa();
	/** @brief Adiciona o apontador ao vector Produtores. @param Novo Produtor @return true se adicionou ou false se nao adicionou **/
	bool addProdutor(Produtor *tempProdutor);
	/** @brief Adiciona o apontador ao vector Clientes. @param Novo Produtor @return true se adicionou ou false se nao adicionou **/
	bool addCliente(Cliente *tempCliente);
	/** @brief Adiciona o apontador ao vector Transacoes. @param Novo Produtor @return true se adicionou ou false se nao adicionou **/
	bool addTransacao(Transacao *tempTransacao);
	/** @brief Adiciona o apontador ao vector Produtos. @param Novo Produtor @return true se adicionou ou false se nao adicionou **/
	bool addProduto(Produto *tempProduto);
	/** @brief Adiciona o apontador ao vector Uvas. @param Novo Produtor @return true se adicionou ou false se nao adicionou **/
	bool addUva(Uva *tempUva);
	/** @brief Adiciona a Empresa à fila de prioridade empresasTransporte. @param Nova EmpresaTransporte @return true se adicionou ou false se nao adicionou **/
	bool addEmpresaTransporte(EmpresaTransporte &tempEmpresa);

	

	/** @brief Elimina o apontador ao vector Clientes. @param Id de um Cliente @return true se eliminou ou false se nao eliminou **/
	bool eliminaCliente(int id);
	/** @brief Elimina o apontador ao vector Produtores. @param Id de um Produtor @return true se eliminou ou false se nao eliminou **/
	bool eliminaProdutor(int id);
	/** @brief Elimina o apontador ao vector Transacao. @param Id de um Cliente @return true se eliminou ou false se nao eliminou **/
	bool eliminaTransacao(int id);
	/** @brief Elimina o apontador ao vector Produtos. @param Id de um Cliente @return true se eliminou ou false se nao eliminou **/
	bool eliminaProduto(int id);
	/** @brief Elimina o apontador ao vector Uvas. @param Id de um Cliente @return true se eliminou ou false se nao eliminou **/
	bool eliminaUva(int id);
	/** @brief Elimina a Empresa da fila de prioridade empresasTransport. @param Id de uma Empresa de Transporte @return true se eliminou ou false se nao eliminou **/
	bool eliminaEmpresaTransporte(int id);
	/** @brief Elimina a Empresa da fila de prioridade empresasTransport. @param Id de uma Empresa de Transporte @return true se eliminou ou false se nao eliminou **/
	bool eliminaViatura(int id);
	/** @brief 'C' - eliminaCliente(id);  'R' - eliminaProdutor(id);  'T' - eliminaTransacao(id); 'P' - eliminaProduto(id); 'U' - eliminaUva(id) @param Opcao @param Id de um Cliente @return true se eliminou ou false se nao eliminou **/
	bool elimina(char op,int id);


	/** @brief Retorna o nome da Cooperativa @return Nome da Cooperativa**/
	string getNome() const								{ return nome; }

	/** @brief Altera o nome da Cooperativa @param Novo Nome da Cooperativa**/
	void setNome(string tempNome);



	/** @brief Retorna um apontador do Tipo Uva que tenha um Id pretendido @param Id da Uva @return Apontador do tipo Uva **/
	Uva* procuraUva(int Id);

	/** @brief Retorna um vector de Uva* que sao todas as uvas separadas por , numa string @param String a tratar @param String fixa/Caracter de Separacao @return Vector de apontadores de tipo Uva**/
	vector <Uva*> procuraUvas(string ids, string needle = ",");


	/** @brief Retorna um apontador do Tipo Produto que tenha um Id pretendido @param Id do Produto @return Apontador do tipo Produto **/
	Produto* procuraProduto(int Id);
	/** @brief Retorna um vector de Produto* que sao todas os produtos separados por , numa string @param String a tratar @param String fixa/Caracter de Separacao @return Vector de apontadores de tipo Produto*/
	vector <Produto*> procuraProdutos(string ids, string needle = ",");
	/** @brief Retorna um apontador do Tipo Produtor que tenha um Id pretendido @param Id do Produtor @return Apontador do tipo Produtor **/
	Produtor* procuraProdutor(int Id);
	/** @brief Retorna um apontador do Tipo Cliente que tenha um Id pretendido @param Id do Cliente @return Apontador do tipo Cliente **/
	Cliente* procuraCliente(int Id);
	/** @brief Retorna um apontador do Tipo Transacao que tenha um Id pretendido @param Id da Transacao @return Apontador do tipo Transacao **/
	Transacao* procuraTransacao(int Id);
	/** @brief Retorna a empresa do Tipo EmpresaTransporte que tenha o Id pretendido @param Id da EmpresaTransporte @return Objecto do tipo EmpresaTransporte **/
	EmpresaTransporte procuraEmpresaTransporte(int Id);


	int arranjaTransporte(double capacidade);


	/** @brief Carrega toda a informacao de um ficheiro txt para os vetores correspondentes **/
	bool carregaFicheiro();
	/** @brief Escreve toda a informacao contida nos vetores para um ficheiro txt **/
	string escreveFicheiro() const;

	class Erro {};

	/** @brief Poe cabeçalho comum a todos os menus e apresenta o erro passado por referência. @param String que contem uma mensagem a mostrar **/
	void menu_cabecalho(string &erro);

	/** @brief Menu a apresentar ao utilizador para carregar ou criar uma cooperativa **/
	void menu_introducao();
	/** @brief Menu que inclui as opcoes para criar uma cooperativa**/
	void menu_criar();
	/** @brief Menu Inicial @param Mensagem a mostrar**/
	void menu_principal(string mensagem = "");
	/** @brief Menu que contem as opcoes para criar diferente objectos **/
	void menu_adicionar();
	/** @brief Menu que contem as opcoes para alterar diferente objectos **/
	void menu_alterar();
	/** @brief Menu que contem as opcoes para listar diferente objectos **/
	void menu_listar();
	/** @brief Menu que contem as opcoes para eliminar diferente objectos **/
	void menu_eliminar();
	/** @brief Menu onde somos interrogados se queremos exportar a informacao para o ficheiro txt **/
	void menu_gravar();
	/** @brief Menu onde somos interrogados se queremos sair do programa @return True se for para sair e false para o caso contrario**/
	bool menu_sair();

	/** @brief Menu para adicionar um Cliente **/
	void menu_adicionarCliente();
	/** @brief Menu para adicionar um Produto **/
	void menu_adicionarProduto();
	/** @brief Menu para adicionar um Produtor **/
	void menu_adicionarProdutor();
	/** @brief Menu para adicionar uma Transacao **/
	void menu_adicionarTransacao();
	/** @brief Menu para adicionar uma Uva **/
	void menu_adicionarUva();
	/** @brief Menu para adicionar uma Viatura **/
	void menu_adicionarViatura();
	/** @brief Menu para adicionar uma Empresa de Transporte **/
	void menu_adicionarEmpresaTransporte();

	/** @brief Menu para alterar um Cliente **/
	void menu_alterarCliente();
	/** @brief Menu para alterar um Produto**/
	void menu_alterarProduto();
	/** @brief Menu para alterar um Produtor **/
	void menu_alterarProdutor();
	/** @brief Menu para alterar uma Transacao **/
	void menu_alterarTransacao();
	/** @brief Menu para adicionar uma Uva **/
	void menu_alterarUva();

	/** @brief Menu para Listar um ou varios Cliente com varias opcoes**/
	void menu_listarCliente();
	/** @brief Menu para Listar um ou varios Produto com varias opcoes**/
	void menu_listarProduto();
	/** @brief Menu para Listar um ou varios Produtor com varias opcoes**/
	void menu_listarProdutor();
	/** @brief Menu para Listar uma ou varios Transacao com varias opcoes**/
	void menu_listarTransacao();
	/** @brief Menu para Listar uma ou varios Uva com varias opcoes**/
	void menu_listarUva();

	void menu_listarEmpresa();

	void menu_listarViatura();

	void menu_listarinativos();

	/** @brief Menu para eliminar um Cliente **/
	void menu_eliminarCliente();
	/** @brief Menu para eliminar um Produto **/
	void menu_eliminarProduto();
	/** @brief Menu para eliminar um Produtor **/
	void menu_eliminarProdutor();
	
	
	/** @brief Menu para adicionar uma Transacao **/
	void menu_eliminarTransacao();
	/** @brief Menu para adicionar uma Uva **/
	void menu_eliminarUva();


	/** @brief Menu para listar todos os clientes **/
	void menu_listarClientesTodos();
	/** @brief Menu para listar todos os clientes compreendidos entre dois ids @param Primeiro id @param Segundo id **/
	void menu_listarClientesEntreId(int n1,int n2);
	/** @brief Menu para listar todos os clientes com o mesmo nome @param Nome a pesquisar**/
	void menu_listarClientesNome(string nome);
	/** @brief Menu para listar todos os produtos **/
	void menu_listarProdutosTodos();
	/** @brief Menu para listar todos os produtos compreendidos entre dois ids @param Primeiro id @param Segundo id**/
	void menu_listarProdutosEntreId(int n1,int n2);
	/** @brief Menu para listar todos os produtos com o mesmo nome @param Nome a pesquisar**/
	void menu_listarProdutosNome(string nome);
	/** @brief Menu para listar todos os produtos que contem determinadas uvas @param Vetor de apontadores para uvas que serve de filtro**/
	void menu_listarProdutosUvas(vector<Uva*> uvas);
	/** @brief Menu para listar todos os produtos que sao produzidos por um produtor @param Id do produtor**/
	void menu_listarProdutosProdutor(int n);
	/** @brief Menu para listar todos os produtos que tem o mesmo tipo @param Tipo do Produto**/
	void menu_listarProdutosTipo(string nome);

	/** @brief Menu para listar todas as Uvas **/
	void menu_listarUvasTodos();
	/** @brief Menu para listar todas as uvas compreendidas entre dois id's @param Primeiro Id @param Segundo Id **/
	void menu_listarUvasEntreId(int n1,int n2);
	/** @brief Menu para listar todas as uvas produzidas por um produtor @param Id do Produtor **/
	void menu_listarUvasProdutor(int id);
	/** @brief Menu para listar todas as uvas com a mesma Casta @param Casta a pesquisar**/
	void menu_listarUvasCasta(string nome);
	/** @brief Menu para listar todas as uvas com a mesma Especie @param Especie a pesquisar **/
	void menu_listarUvasEspecie(string nome);
	/** @brief Menu para listar todas as uvas com a mesma Regiao @param Regiao a pesquisar **/
	void menu_listarUvasRegiao(string nome);
	/** @brief Menu para listar todas as uvas com a mesma cor @param Cor a pesquisar **/
	void menu_listarUvasCor(string nome);
	/** @brief Menu para listar todas as uvas com o mesmo aroma @param Aroma a pesquisar **/
	void menu_listarUvasAroma(string nome);
	/** @brief Menu para listar todas as uvas compreendidas entre dois precos @param Primeiro Valor @param Segundo Valor **/
	void menu_listarUvasEntrePreco(double n1,double n2);

	/** @brief Menu para listar todos os Produtoresa **/
	void menu_listarProdutorTodos();
	/** @brief Menu para listar todos os produtores compreendidos entre dois id's @param Primeiro Id @param Segundo Id **/
	void menu_listarProdutorEntreId(int n1,int n2);
	/** @brief Menu para listar todos os Produtores com o mesmo nome @param Nome a pesquisar **/
	void menu_listarProdutorNome(string nome);
	/** @brief Menu para listar todos os produtores que produzem determinadas uvas @param Vetor de apontadores do tipo Uva que serve de filtro **/
	void menu_listarProdutorUvas(vector<Uva*> uvas);
	/** @brief Menu para listar todos os produtores que produzem determinados produtos @param Vetor de apontadores do tipo Produto que serve de filtro **/
	void menu_listarProdutorProdutos(vector<Produto*> produtos);
	/** @brief Menu para listar todas as transaccoes **/
	void menu_listarTransacaoTodos();
	/** @brief Menu para listar todas as transaccoes compreendidas entre duas datas @param Data 1 @param Data 2 **/
	void menu_listarTransacaoEntreData(std::time_t n1,std::time_t n2);
	/** @brief Menu para listar todas as transaccoes compreendidas entre dois Id's @param Primeiro Id @param Segundo Id **/
	void menu_listarTransacaoEntreId(int n1,int n2);
	/** @brief Menu para listar todas as transaccoes de um Cliente @param id do Cliente**/
	void menu_listarTransacaoComprador(int n1);
	/** @brief Menu para listar todas as transaccoes de um Produtor @param id do Produtor**/
	void menu_listarTransacaoVendedor(int n1);
	/** @brief Menu para listar todas as transaccoes de um conjunto de Produtos @param Vetor de apontadores do tipo Produto que serve de filtro**/
	void menu_listarTransacaoProdutos(vector<Produto*> produtos);


	void menu_listarEmpresaTodos();
	void menu_listarEmpresaEntreId(int n1, int n2);
	void menu_listarEmpresaNome(string nome);
	void menu_listarEmpresaCapacidade(double n);


	void menu_listarViaturaTodos();
	void menu_listarViaturaEmpresa(int id);
	void menu_listarViaturaCapacidade(double n);

	/** @brief actualiza a carteira de clientes inactivos **/
	void actualizainativos();
	/** @brief elimina o cliente c1 da tabela de clientes inactivos @param cliente a eliminar **/
	bool eliminainativo(Cliente c1);
	/** @brief mostra a tabela de clientes inactivos **/
	void listarinativos(); //Não acabado. Falta operador
};


#endif // COOPERATIVA_H_INCLUDED
