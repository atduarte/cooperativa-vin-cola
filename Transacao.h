#ifndef TRANSACAO_H_INCLUDED
#define TRANSACAO_H_INCLUDED

#include <map>
#include <ctime>

#include "cliente.h"
#include "produtor.h"
#include "empresatransporte.h"
#include "utilidades.h"


class Transacao {
	friend class Cooperativa;
    int id;
    static int ultimoId;
	std::time_t data; //timestamp
	map<Produto *, int> produtos;
	float montante;
	Cliente* comprador;
    Produtor* vendedor;
	int idEmpresaTransporte;
public:
   Transacao(int tempId, std::time_t tempTimestamp, Cliente* tempCliente, Produtor* tempProdutor, map<Produto *, int> tempProdutos, float tempMontante, int tempIdEmpresaTransporte = -1);
   Transacao(std::time_t tempTimestamp, Cliente* tempCliente, Produtor* tempProdutor, map<Produto *, int> tempProdutos, float tempMontante, int tempIdEmpresaTransporte = -1);
   Transacao();
   
   /** @brief Retorna Id da Transacao @return id **/
   int getId() const						{ return id; }
   /** @brief Retorna data da Transacao @return data **/
   std::time_t getData() const				{ return data; }
   /** @brief Retorna Id do Cliente  @return cliente id **/
   int getClienteId() const					{ return comprador->getId(); }
   /** @brief Retorna Id do Vendedor @return Vendedor id **/
   int getVendedorId() const				{ return vendedor->getId(); }
   /** @brief Retorna Produtos e Quantidade @return produtos e quantidade **/
   const map<Produto* , int>& getProdutos() const	{ return produtos; }
   /** @brief Retorna Numero de Produtos @return Numero de Produtos da Transacao **/
   int getProdutosSize() const				{ return produtos.size(); }
   /** @brief Retorna Montante @return montante **/
   float getMontante() const				{ return montante; }
    /** @brief Retorna id da empresa de transporte existente na transacao @return id **/
   int getEmpresaTransporteId() const		{ return idEmpresaTransporte; }

   /** @brief Elimina Produto com id dado. @param Id do Produto @return true se encontra, false caso contrário **/
   bool eliminarProdId(int id);

   /** @brief Muda o Comprador @param Apontador para o Comprador **/
   void setComprador(Cliente* Comprador)				{ comprador = Comprador; }
   /** @brief Muda o Vendedor @param Apontador para o Vendedor **/
   void setVendedor(Produtor* Vendedor)					{ vendedor = Vendedor; }
   /** @brief Adiciona Produto e respectiva quantidade. @param Apontador para Produto. @param Quantidade **/
   void addProduto(Produto* produto, int quantidade)	{ produtos[produto] = quantidade; }
   /** @brief Muda Data da transacao @param Timestamp da Nova data **/
   void setData(time_t Data)							{ data = Data; }

   friend ostream & operator<<(ostream &o, Transacao &t);
};

#endif // TRANSACAO_H_INCLUDED

