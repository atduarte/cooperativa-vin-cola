#ifndef UVA_H_
#define UVA_H_

#include <string>

#include "utilidades.h"

using namespace std;

class Uva {
	int id;
	static int ultimoId;
	string casta;
	string especie;
	string regiao;
	string cor;
	string aroma;
	float preco; //necessario para calcular o preco de um vinho //Preco por litro!
public:
	Uva();
	Uva(int Id, string Casta, string Especie, string Regiao, string Cor, string Aroma, float Preco);
	Uva(string Casta, string Especie, string Regiao, string Cor, string Aroma, float Preco);

    /** @brief Retorna Id @return id **/
	int getId() const				{ return id; }
    /** @brief Retorna Casta @return casta **/
	string getCasta() const			{ return casta; }
    /** @brief Retorna Especie @return especie **/
	string getEspecie() const		{ return especie; }
	/** @brief Retorna Regiao @return Regiao **/
	string getRegiao() const		{ return regiao; }
	/** @brief Retorna Cor @return Cor **/
	string getCor() const			{ return cor; }
	/** @brief Retorna Aroma @return aroma **/
	string getAroma() const			{ return aroma; }
	/** @brief Retorna preco @return preco **/
	double getPreco() const			{ return preco; }

	 /** @brief Muda Casta @param  @return casta **/
	void setCasta(string Casta)		{ casta = Casta; }
	 /** @brief Muda Especie @param  @return especie **/
	void setEspecie(string Especie) { especie = Especie; }
	 /** @brief Muda Regiao @param  @return regiao **/
	void setRegiao(string Regiao)	{ regiao = Regiao; }
	 /** @brief Muda Cor @param  @return cor **/
	void setCor(string Cor)			{ cor = Cor; }
	 /** @brief Muda Aroma @param  @return aroma **/
	void setAroma(string Aroma)		{ aroma = Aroma; }
	 /** @brief Muda Preco @param  @return preco **/
	void setPreco(double Preco)		{ preco = Preco; }

	friend ostream & operator<<(ostream &o, Uva &u);
	
};

#endif /* UVA_H_ */