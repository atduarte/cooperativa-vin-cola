#include "empresatransporte.h"

int EmpresaTransporte::ultimoId = 0;

EmpresaTransporte::EmpresaTransporte(int Id, string Nome, double Distancia, list <Viatura> Viaturas): id(Id), nome(Nome), distancia(Distancia), viaturas(Viaturas)
{
	if(Id > ultimoId)
		ultimoId = Id +1;
}

EmpresaTransporte::EmpresaTransporte(string Nome, double Distancia, list <Viatura> Viaturas): nome(Nome), distancia(Distancia), viaturas(Viaturas)
{
	id = ultimoId;
	ultimoId++;
}

void EmpresaTransporte::adicionaViatura(Viatura tmpViatura) 
{
	list< Viatura >::iterator it;

	for( it = viaturas.begin(); it != viaturas.end(); it++)
	{
		if( (*it).getCapacidade() <= tmpViatura.getCapacidade() ) {
			viaturas.insert(it, tmpViatura);
			return;
		}
	}

	viaturas.push_back(tmpViatura);
}

bool EmpresaTransporte::removeViatura(int Id)
{
	list< Viatura >::iterator it;

	for( it = viaturas.begin(); it != viaturas.end(); it++)
	{
		if( (*it).getId() <= Id ) {
			it = viaturas.erase(it);
			return true;
		}
	}
	return false;
}

bool EmpresaTransporte::operator<(const EmpresaTransporte & e2) const
{
	return !(distancia < e2.getDistancia());
}

ostream & operator<<(ostream &o, EmpresaTransporte &c)
{
	stringstream ss;
	ss << "Id: " << c.getId() 
	   << " | Nome: " << c.getNome() 
	   << " | Distancia: " << c.getDistancia() 
	   << " | Ids das Viaturas: ";

	list<Viatura> tempViaturas = c.getViaturas();
	list<Viatura>::const_iterator it;
	for(it = tempViaturas.begin(); it != tempViaturas.end(); it++)
	{
		ss << (*it).getId() << " ";
	}

	o << center(ss.str());

	return o;
}

double EmpresaTransporte::getCapacidade() const
{
	double total = 0;
	list <Viatura> tempViaturas = viaturas;
	list <Viatura>::iterator it;

	for(it = tempViaturas.begin(); it != tempViaturas.end(); it++)
	{
		total += (*it).getCapacidade();
	}

	return total;


}