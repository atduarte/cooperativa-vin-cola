#include "utilidades.h"

template<typename T>
T StringToNumber(const string& numberAsString)
{
	T valor;

	std::stringstream stream(numberAsString);
	stream >> valor;
	
	return valor;
}

string IntToString ( int Number )
{
	stringstream ss;
	ss << Number;
	return ss.str();
}

string DoubleToString ( double Number )
{
	stringstream ss;
	ss << Number;
	return ss.str();
}

template<typename G>
string NumberToString(G number) {
	stringstream ss;
	ss << Number;
	return ss.str();
}

int stringCutId(string& original) 
{
	//Copiar Id do inicio
	int pos = original.find(" ");
	string id = original.substr(0,pos);

	//Eliminar do original
	original.erase(0, ++pos);

	return atoi(id.c_str());
}

std::time_t stringCutTime(string& original) 
{
	//Copiar Id do inicio
	int pos = original.find(" ");
	string id = original.substr(0,pos);

	//Eliminar do original
	original.erase(0, ++pos);

	return atof(id.c_str());
}

string stringCutConteudo(string& original, string inicial, string final)
{
	//Eliminar primeiro { e o que est� para tras
	int pos = original.find(inicial);
	original.erase(0,++pos);

	//Copiar conteudo e eliminar do original
	pos = original.find(final);
	string content = original.substr(0,pos);
	original.erase(0, pos+2);

	return content;
}

vector<int> explode(string original, string separador)
{
	vector<int> resultado;
	int pos;
	string content;
	bool continuar = true; 

	while(continuar) {
		pos = original.find(separador);
		if(pos == string::npos)
			continuar = false;

		content = original.substr(0,pos);
		original.erase(0, ++pos);

		if(content != "")
			resultado.push_back( atoi(content.c_str()) );
	}

	return resultado;
}

vector< vector<float> > explodeD(string original, string separador, string separador2)
{
	vector<string> preresultado;
	vector< vector<float> > resultado;
	int pos;
	string content;
	bool continuar = true; 

	while(continuar) {
		pos = original.find(separador);
		if(pos == string::npos)
			continuar = false;

		content = original.substr(0,pos);
		original.erase(0, ++pos);

		if(content != "") {
			//Tirar primeiro e ultimo caracter
			content = content.substr(1, content.size()-2);
			preresultado.push_back( content );
		}
	}

	for(unsigned int i = 0; i < preresultado.size(); i++)
	{
		pos = preresultado[i].find(separador2);
		
		original = preresultado[i];

		//Separar valores
		content = preresultado[i].substr(0, pos);
		original.erase(0, ++pos);

		//Criar vector temporario com id e quantidade
		vector<float> temp;
		temp.push_back( atoi(content.c_str()) );
		temp.push_back( atof(original.c_str()) );

		//Por esse vector no vector de vectores - no resultado
		resultado.push_back(temp);
	}

	return resultado;
}

void stringCenter(string s, bool newline)
{
   int l = s.size();
   int pos=(int)((80-l)/2);
   for(int i=0;i<pos;i++)
	 cout << " ";

   cout << s;
   if(newline)
	   cout << endl;
}


string center(string s)
{
	stringstream ss;
	int l = s.size();
	int pos=(int)((80-l)/2);
	for(int i=0;i<pos;i++)
		ss << " ";

	ss << s;

	return ss.str();
}


/* 

vector< vector<float> > explodeF(string original, string separador, string separador2)
{
	vector<string> preresultado;
	vector<float> temp;
	vector< vector<float> > resultado;
	int pos;
	string content;
	bool continuar = true; 

	while(continuar) {
		pos = original.find(separador);
		if(pos == string::npos)
			continuar = false;

		content = original.substr(0,pos);
		original.erase(0, ++pos);

		if(content != "") {
			//Tirar primeiro e ultimo caracter
			content = content.substr(1, content.size()-2);
			preresultado.push_back( content );
		}
	}

	for(unsigned int i = 0; i < preresultado.size(); i++)
	{
		pos = preresultado[i].find(separador2);

		//Separar valores
		content = original.substr(1,pos);
		original.erase(0, ++pos);

		//Criar vector temporario com id e quantidade
		temp.empty();
		temp.push_back( atoi(content.c_str()) );
		temp.push_back( atof(original.c_str()) );

		//Por esse vector no vector de vectores - no resultado
		resultado.push_back(temp);
	}

	return resultado;
}*/