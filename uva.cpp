#include "uva.h"

int Uva::ultimoId = 0;

Uva::Uva()
{
	ultimoId++;
	id = ultimoId;
}

Uva::Uva(string Casta, string Especie, string Regiao, string Cor, string Aroma, float Preco)
{
	casta = Casta;
	especie = Especie;
	regiao = Regiao;
	cor = Cor;
	aroma = Aroma;
	preco = Preco;

	ultimoId++;
	id = ultimoId;
}

Uva::Uva(int Id, string Casta, string Especie, string Regiao, string Cor, string Aroma, float Preco)
{
	casta = Casta;
	especie = Especie;
	regiao = Regiao;
	cor = Cor;
	aroma = Aroma;
	preco = Preco;

	if(Id > ultimoId)
		ultimoId = Id;
	// Se for menor devia-se verificar se j� existe...

	id = Id;
}

ostream & operator<<(ostream &o, Uva &u)
{
	stringstream ss;
	ss << "Id: " << u.getId()
	   << " | Aroma: " << u.getAroma()
	   << " | Casta: " << u.getCasta()
	   << " | Cor: " << u.getCor()
	   << " | Especie: " << u.getEspecie()
	   << " | Regiao: " << u.getRegiao()
	   << " | Preco: " << u.getPreco();

	o << center(ss.str());

	return o;
}