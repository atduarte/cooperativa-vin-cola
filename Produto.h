#ifndef PRODUTO_H_INCLUDED
#define PRODUTO_H_INCLUDED

#include <string>
#include <vector>
#include <ctime>
#include <map>
#include <sstream>
#include "uva.h"
#include "utilidades.h"

using namespace std;

class Produto {
	int id;
	static int ultimoId;
	map<Uva *, float> uvas; //Uva e a sua percentagem no produto.
	string nome;
protected:
	float preco;
public:
	Produto();
	Produto(string Nome);
	Produto(string Nome, map<Uva *, float> Uvas);
	Produto(int Id, string Nome, map<Uva *, float> Uvas);
	
	/** @brief Operador menor definido para a BST. @param Produto a comparar.**/
	bool operator < (const Produto & p) const;
	/** @brief Operador igual igual definido para a BST. @param Produto a comparar.**/
	bool operator == (const Produto & p) const;


	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual float calculaPreco() const = 0;
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual map<string, string> getAtributos() const = 0;
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual string getTipo() const = 0;
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual string getSubTipo() const = 0;
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual int getAno() const = 0;
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual void setSubTipo(string SubTipo) = 0;
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual void setAnoColheita(int AnoColheita) { };
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual void setVolumeAlcoolico(double VolumeAlcoolico) { };
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual void setTempoArmazenamento(int TempoArmazenamento) { };
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual void setVolumeCo2(int VolumeCo2) { };
	/** @brief Fun��o virtual, redefinida nas classes derivadas.**/
	virtual void setQualidadeCo2(string QualidadeCo2) { };
	

	/** @brief Retorna o Id de um Produto.@return int com o Id do Produto. **/
	int		getId() const					 { return id;			}
	/** @brief Retorna o nome de um Produto.@return string com o nome do Produto. **/
	string	getNome() const					 { return nome;			}
	/** @brief Retorna o map de Uvas e a sua quantidade num Produto.@return map com Uvas e suas quantidades do Produto. **/
	const map<Uva *, float>& getUvas() const { return uvas;			}
	/** @brief Retorna um vector com as Uvas de um Produto.@return vector com as uvas do Produto. **/
	vector<Uva *> getUvasVec() const;
	/** @brief Retorna o numero de uvas de um Produto.@return int com o numero de uvas do Produto. **/
	int		getUvasSize() const				 { return uvas.size();	}	
	/** @brief Retorna o Id de uma Uva do Produto.@return int com o Id da Uva do Produto. **/
	int		getUvaId(int pos) const;
	/** @brief Retorna a Cor de uma de um Produto, para efeitos da determina��o do tipo de vinho. @return string com a cod da Uva do Produto. **/
	string	getUvaCor(int pos) const;
	/** @brief Retorna o PReco de um Produto. @return float com o Preco do Produto. **/
	float	getPreco() const {return preco;}
	
	/** @brief Altera o preco do produto para o novo nome.@param float com o novo preco. **/
	void setPreco(float prc) {prc = preco;}
	/** @brief Altera o nome do produto para o novo nome.@param string com o novo nome. **/										 
	void	setNome(string Nome)			 { nome = Nome; }
	/** @brief Adiciona Uva e a sua quantidade no map de uvas.@param Apontador para uma Uvas e a sua quantidade, em litros, no produto. **/
	void	addUva(Uva *tempUva, float percentagem);
	/** @brief Procura se uma Uva existe no Produto.@Retorna true se certa uma existir no Produto**/
	bool	procuraUva(int id)	const;

	/**@brief Retorna um ostream para que os dados sejam escritos na consola @param ostream e o Produto a escrever @return ostream com os dados **/ 
	friend ostream & operator<<(ostream &o, Produto &p) {return o<<center(p.print());};
    virtual string print();
};

class ProdutoPtr {
	Produto * Prod;
public:
	ProdutoPtr(Produto * ptr) {Prod = ptr;}

	/** @brief Retorna o apontador da classe.@return apontador para um produto. **/	
	Produto * getPtr() const {return Prod;}
	/** @brief compara dois produtos com base nos apontador @param apontador a comparar @return booleana com o resultado da compara��o **/
	bool operator < (const ProdutoPtr & p) const;
};


class Vinho: public Produto {
	static string tipo;
	string subTipo;
	int anoColheita;
	double volumeAlcoolico;
public:
	Vinho(string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico);
	Vinho(int Id, string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico);

	
	/** @brief Calcula o pre�o de um vinho do porto mediante as uvas que este possui, as suas quantidades e o tempo de aramazenamento .@return float com o pre�o do vinho. **/
	float calculaPreco() const;
	/** @brief Retorna os atributos de um vinho.@return Retorna um map com todos os membros-dado do vinho. **/
	map<string, string> getAtributos() const;
	/** @brief Altera o subtipo do vinho. @param novo subtipo do vinho **/
	void setSubTipo(string SubTipo) { subTipo = SubTipo; }
	/** @brief Altera o ano de colheita do vinho. @param novo ano de colheita do vinho **/
	void setAnoColheita(int AnoColheita) { anoColheita = AnoColheita; }
	/** @brief Altera o volume alcoolico do vinho. @param novo volume alcoolico do vinho **/
	void setVolumeAlcoolico(double VolumeAlcoolico) { volumeAlcoolico = VolumeAlcoolico; }
	/** @brief Altera o tempo de armazenamento do vinho (funcao virtual). @param int com o novo tempo. **/
	void setTempoArmazenamento(int TempoArmazenamento) { };

	/** @brief Retorna o ano de colheita do vinho.@return int com o ano de colheita do vinho. **/		
	int		getAno() const					{ return anoColheita;}
	/** @brief Retorna o volume alcoolico do vinho .@return double com o volume alcoolico. **/		
	double	getVolume() const				{ return volumeAlcoolico;}
	/** @brief Retorna o tipo do vinho (funcao virtual).@return tipo do vinho. **/		
	string	getTipo() const					{ return tipo;}
	/** @brief Retorna o subtipo do vinho .@return subtipo do vinho. **/		
	string  getSubTipo() const				{ return subTipo; }	

	virtual string print();
};


class VinhoDoPorto: public Vinho {
	static string tipo;
	int tempoArmazenamento;
public: 
	VinhoDoPorto(string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico, int TempoArmazenamento);
	VinhoDoPorto(int Id, string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico, int TempoArmazenamento);

	/** @brief Calcula o pre�o de um vinho do porto mediante as uvas que este possui, as suas quantidades e o tempo de aramazenamento .@return float com o pre�o do vinho do porto. **/
	float calculaPreco() const;
	/** @brief Retorna os atributos de um vinho.@return Retorna um map com todos os membros-dado do vinho. **/
	map<string, string> getAtributos() const;
	/** @brief Altera o tempo de armazenamento do vinho. @param int com o novo tempo. **/		
	void setTempoArmazenamento(int TempoArmazenamento) { tempoArmazenamento = TempoArmazenamento; }

	/** @brief Retorna o tipo do vinho do porto (funcao virtual).@return tipo do vinho do porto. **/		
	string	getTipo() const					{ return tipo;}
	/** @brief Retorna o tempo de aramazenamento do vinho do porto. @return com o tempo de armazenamento do vinho. **/		
	int		getTempo() const				{ return tempoArmazenamento; }
	
	string print();
};


class Espumante: public Vinho {
	static string tipo;
	double volumeCo2;
	string qualidadeCo2;
public:
	Espumante(string Nome, map<Uva*,float> Uvas, int AnoColheita, double VolumeAlcoolico, double VolumeCo2, string QualidadeCo2);
	Espumante(int Id, string Nome, map<Uva*,float> Uvas, int AnoColheita, double VolumeAlcoolico, double VolumeCo2, string QualidadeCo2);

	/** @brief Calcula o pre�o de um vinho do porto mediante as uvas que este possui, as suas quantidades, volume e qualidade do Co2 .@return float com o pre�o do espumante. **/
	float calculaPreco() const;
	/** @brief Retorna os atributos de um espumante.@return Retorna um map com todos os membros-dado do espumante. **/
	map<string, string> getAtributos() const;
	/** @brief Recebe o volume de Co2 do espumante .@return double com o volume. **/	
	void setVolumeCo2(int VolumeCo2)				{volumeCo2 = VolumeCo2; }
	/** @brief Recebe a qualidade do espumante .@return string com a Qualidade do Co2 do espumante. **/	
	void setQualidadeCo2(string QualidadeCo2)		{qualidadeCo2 = QualidadeCo2; }

	/** @brief Altera o subtipo do vinho. @param novo subtipo do vinho **/
	string getTipo() const					{return tipo; }
	/** @brief Retorna o volume de Co2 (funcao virtual) .@return volume de co2 no espumante. **/	
	double getCo2() const					{return volumeCo2;}
	/** @brief Retorna a qualidade de Co2 (funcao virtual) .@return qualidade de co2 do espumante. **/	
	string getQualidade() const				{return qualidadeCo2;}

	string print();
};

class Derivado: public Produto {
	static string tipo;
	string subTipo;
public:
	Derivado(string Nome, map<Uva*,float> Uvas, string SubTipo, float Preco);
	Derivado(int Id, string Nome, map<Uva*,float> Uvas, string SubTipo, float Preco);

	/** @brief Calcula o pre�o de um vinho do porto mediante o seu tipo .@return float com o pre�o do derivado. **/
	float calculaPreco() const;
	/** @brief Retorna os atributos de um derivado .@return Retorna um map com todos os membros-dado do derivado. **/
	map<string, string> getAtributos() const;
	/** @brief Altera o subtipo do derivado. @param novo subtipo do derivado **/
	void setSubTipo(string SubTipo) { subTipo = SubTipo; }
	/** @brief Altera o preco do derivado. @param novo preco do derivado **/
	void setPreco(float preco) { this->preco = preco; }
	/** @brief Retorna  o tipo do derivado. @return subtipo do derivado **/
	string getTipo() const					{return tipo; }
	/** @brief Retorna  o subtipo do derivado. @return tipo do derivado **/
	string getSubTipo() const				{return subTipo; }
	/** @brief Retorna  o ano de colheita do derivado. Colocado a -1 para que o operador < fosse executado plenamente @return ano de colheita do derivado **/
	int getAno() const {return -1;}

	string print();
};

#endif // PRODUTO_H_INCLUDED

