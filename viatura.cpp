#include "viatura.h"

int Viatura::ultimoId = 0;

Viatura::Viatura(int Id, double Capacidade): id(Id), capacidade(Capacidade) 
{
	if(Id > ultimoId)
		ultimoId = Id +1;
	//Devia-se verificar se existe repeti��o....
}

Viatura::Viatura(double Capacidade): capacidade(Capacidade) 
{
	id = ultimoId;
	ultimoId++;
}

ostream & operator<<(ostream &o, Viatura &v)
{
	stringstream ss;
	ss << "Id: " << v.getId() 
	   << " | Capacidade: " << v.getCapacidade();


	o << center(ss.str());

	return o;
}