#include "produto.h"

//ProdutoPtr

bool ProdutoPtr:: operator < (const ProdutoPtr & p) const
{
	if ( Prod->getNome() < p.getPtr()->getNome())
		return true;
	else if ( Prod->getNome() == p.getPtr()->getNome())
	{
		if (Prod->getTipo() < p.getPtr()->getTipo())
			return true;
		else if (Prod->getTipo() == p.getPtr()->getTipo())
		{
			if(Prod->getSubTipo() < p.getPtr()->getSubTipo())
				return true;
			else if (Prod->getSubTipo() == p.getPtr()->getSubTipo())
			{
				if (Prod->getAno() > p.getPtr()->getAno())
					return true;
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	
	}
	else
		return false;
}



//Produto

int Produto::ultimoId = 0;

Produto::Produto()
{
	ultimoId++;
	id = ultimoId;
}

Produto::Produto(string Nome)
{
    nome = Nome;
	ultimoId++;
	id = ultimoId;
}

Produto::Produto(string Nome, map<Uva *, float> Uvas) 
{
	uvas = Uvas;
	nome = Nome;

	ultimoId++;
	id = ultimoId;
}

Produto::Produto(int Id, string Nome, map<Uva *, float> Uvas) 
{
	if(Id > ultimoId)
		ultimoId = Id;
	// Se for menor devia-se verificar se j� existe...

	id = Id;

	uvas = Uvas;
	nome = Nome;
}

void Produto::addUva(Uva *tempUva, float litros)
{
	uvas[tempUva] = litros;
}

int Produto::getUvaId(int pos) const
{
	map<Uva *, float>::const_iterator it = uvas.begin();
	advance (it, pos);
	return (*it).first->getId();
}

string Produto::getUvaCor(int pos) const
{
	map<Uva *, float>::const_iterator it = uvas.begin();
	advance (it, pos);
	return (*it).first->getCor();
}

bool Produto::procuraUva(int id) const {
	map<Uva *, float>::const_iterator it;
	for(it = uvas.begin(); it != uvas.end(); it++)
		if( (*it).first->getId() == id)
			return true;

	return false;

}

vector<Uva *> Produto::getUvasVec() const {
	vector<Uva *> ret;
	map<Uva*, float>::const_iterator it;
	for(it = uvas.begin(); it != uvas.end(); it++)
		ret.push_back( (*it).first );
	return ret;
}

bool Produto::operator < (const Produto & p) const
{
	if( preco < p.getPreco())
		return true;
	else
		return false;
}

bool Produto::operator == (const Produto & p) const
{
	if( preco == p.getPreco())
		return true;
	else
		return false;
}

//Vinho

string Vinho::tipo = "Vinho";

Vinho::Vinho(string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico): Produto(Nome, Uvas)
{
	subTipo = SubTipo;
	anoColheita		= AnoColheita;
	volumeAlcoolico	= VolumeAlcoolico;
	preco = calculaPreco();
}

Vinho::Vinho(int Id, string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico): Produto(Id, Nome, Uvas)
{
	subTipo = SubTipo;
	anoColheita		= AnoColheita;
	volumeAlcoolico	= VolumeAlcoolico;
	preco = calculaPreco();
}

//O Preco do vinho e calcula pelo preco de um litro de "liquido" de uma certa uva vezes a quantidade em litros dessa uva no vinho.
float Vinho::calculaPreco() const 
{
	float preco = 0;

	for(map<Uva *, float>::const_iterator it = getUvas().begin(); it != getUvas().end(); it++)
	{
		preco += (*it).first->getPreco() * (*it).second;
	}

	return preco;

}

map<string, string> Vinho::getAtributos() const 
{
	map<string, string> ret;

	ret["tipo"] = tipo;
	ret["subTipo"] = subTipo;
	ret["anoColheita"] = IntToString(anoColheita);
	ret["volumeAlcoolico"] = DoubleToString(volumeAlcoolico);

	return ret;
}

//Vinho do Porto

string VinhoDoPorto::tipo = "VinhoDoPorto";

VinhoDoPorto::VinhoDoPorto(string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico, int TempoArmazenamento): Vinho(Nome, Uvas, SubTipo, AnoColheita, VolumeAlcoolico)
{
	tempoArmazenamento = TempoArmazenamento;
	preco = calculaPreco();
}

VinhoDoPorto::VinhoDoPorto(int Id, string Nome, map<Uva*,float> Uvas, string SubTipo, int AnoColheita, double VolumeAlcoolico, int TempoArmazenamento): Vinho(Id, Nome, Uvas, SubTipo, AnoColheita, VolumeAlcoolico)
{
	tempoArmazenamento = TempoArmazenamento;
	preco = calculaPreco();
}

float VinhoDoPorto::calculaPreco() const
{
	float preco = 0;

	for(map<Uva *, float>::const_iterator it = getUvas().begin(); it != getUvas().end(); it++)
	{
		preco += (*it).first->getPreco() * (*it).second;
	}

	preco = (preco*0.1*tempoArmazenamento);
	return preco;
}

map<string, string> VinhoDoPorto::getAtributos() const
{
	map<string, string> ret;
	ret["tipo"]					= getTipo();
	ret["subTipo"]				= getSubTipo();
	ret["anoColheita"]			= IntToString(getAno());
	ret["volumeAlcoolico"]		= DoubleToString(getVolume());
	ret["tempoArmazenamento"]	= IntToString(tempoArmazenamento);

	return ret;
}

//Espumante

string Espumante::tipo = "Espumante";

Espumante::Espumante(string Nome, map<Uva*,float> Uvas, int AnoColheita, double VolumeAlcoolico, double VolumeCo2, string QualidadeCo2):Vinho(Nome, Uvas, "", AnoColheita, VolumeAlcoolico)
{
	volumeCo2 = VolumeCo2;
	qualidadeCo2 = QualidadeCo2;
	preco = calculaPreco();
}

Espumante::Espumante(int Id, string Nome, map<Uva*,float> Uvas, int AnoColheita, double VolumeAlcoolico, double VolumeCo2, string QualidadeCo2):Vinho(Id, Nome, Uvas, "", AnoColheita, VolumeAlcoolico)
{
	volumeCo2 = VolumeCo2;
	qualidadeCo2 = QualidadeCo2;
	preco = calculaPreco();
}

float Espumante::calculaPreco() const
{
	float preco = 0;

	for(map<Uva *, float>::const_iterator it = getUvas().begin(); it != getUvas().end(); it++)
	{
		preco += (*it).first->getPreco() * (*it).second;
	}

	if (qualidadeCo2 == "Bom")
		preco = preco * 0.2;
	else if (qualidadeCo2 == "Medio")
		preco = preco * 0.1;
	else if (qualidadeCo2 == "Fraco")
		preco = preco * 0.05;

	preco = preco * volumeCo2;
	return preco;
}

map<string, string> Espumante::getAtributos() const
{
	map<string, string> ret;

	ret["tipo"]					= getTipo();
	ret["anoColheita"]			= IntToString(getAno());
	ret["volumeAlcoolico"]		= DoubleToString(getVolume());
	ret["volumeCo2"] = DoubleToString(volumeCo2);
	ret["qualidadeCo2"] = qualidadeCo2;

	return ret;
}

//Derivado

string Derivado::tipo = "Derivado";

Derivado::Derivado(string Nome, map<Uva*,float> Uvas, string SubTipo, float Preco):Produto(Nome, Uvas)
{
	subTipo = SubTipo;
	preco = calculaPreco();
}

Derivado::Derivado(int Id, string Nome, map<Uva*,float> Uvas, string SubTipo, float Preco):Produto(Id, Nome, Uvas)
{
	subTipo = SubTipo;
	preco = Preco;
}

float Derivado::calculaPreco() const
{
	return preco;
}

map<string, string> Derivado::getAtributos() const
{
	map<string, string> ret;
	ret["tipo"] = tipo;
	ret["subTipo"] = subTipo;

	stringstream ss;
	ss << preco;
	ret["preco"] = ss.str();
	return ret;
}

//ESPUMANTE
string Espumante::print()
{
        stringstream ss;
        ss<<"Id: "<<this->getId()<<" | Nome: "<<getNome()<<" | Preco: "<<preco<<" | Tipo: "<<getTipo()<<" | Ano de Colheita: "<<getAno()<<" | Volume Alcoolico: "<<getVolume()<<" | VolumeCo2 "<<getCo2()<<" | QualidadeCo2 "<<getQualidade()<<" | Uvas: ";
        vector<Uva *> tempuvas=getUvasVec();
        for(int i=0;i<tempuvas.size();i++)
        {
                ss<<tempuvas[i]->getId()<<" ";
        }
        return ss.str();
}
//VINHO
string Vinho::print()
{
        stringstream ss;
        ss<<"Id: "<<getId()<<" | Nome: "<<getNome()<<" | Preco: "<<getPreco()<<" | SubTipo: "<<getSubTipo()<<" | Ano de Colheita: "<<getAno()<<" | Volume Alcoolico: "<<getVolume()<<" | Uvas: ";
        vector<Uva *> tempuvas=getUvasVec();
        for(int i=0;i<tempuvas.size();i++)
        {
                ss<<tempuvas[i]->getId()<<" ";
        }
        return ss.str();
}
//PRODUTO
string Produto::print()
{
        stringstream ss;
        ss<<"Id: "<<getId()<<" | Nome: "<<getNome()<<" | Preco: "<<getPreco()<<" | Uvas: ";
        vector<Uva *> tempuvas=getUvasVec();
        for(int i=0;i<tempuvas.size();i++)
        {
                ss<<tempuvas[i]->getId()<<" ";
        }
        return ss.str();
       
}
//DERIVADO
string Derivado::print()
{
        stringstream ss;
        ss<<"Id: "<<getId()<<" | Nome: "<<getNome()<<" | Preco: "<<getPreco()<<" | SubTipo: "<<getSubTipo()<<" | Tipo "<<getTipo()<<" | Uvas: ";
        vector<Uva *> tempuvas=getUvasVec();
        for(int i=0;i<tempuvas.size();i++)
        {
                ss<<tempuvas[i]->getId()<<" ";
        }
        return ss.str();
}
//Vinho do Porto
string VinhoDoPorto::print()
{
        stringstream ss;
        ss<<"Id: "<<getId()<<" | Nome: "<<getNome()<<" | Preco: "<<getPreco()<<" | Tipo: "<<getTipo()<<" | Ano de Colheita: "<<getAno()<<" | Volume Alcoolico: "<<getVolume()<<" | Tempo de Armazenamento "<<getTempo()<<" | Uvas: ";
        vector<Uva *> tempuvas=getUvasVec();
        for(int i=0;i<tempuvas.size();i++)
        {
                ss<<tempuvas[i]->getId()<<" ";
        }
       
        return ss.str();
}