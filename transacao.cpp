#include "Transacao.h"

int Transacao::ultimoId = 0;

Transacao::Transacao(int tempId, std::time_t tempTimestamp, Cliente* tempCliente, Produtor* tempProdutor, map<Produto *, int> tempProdutos, float tempMontante, int tempIdEmpresaTransporte) {

	if(tempId > ultimoId)
		ultimoId = tempId;
	//Devia-se verificar se existem repetições

	data = tempTimestamp;
	comprador = tempCliente;
	vendedor = tempProdutor;
	produtos = tempProdutos;
	montante = tempMontante;

	id = tempId;

	idEmpresaTransporte = tempIdEmpresaTransporte;
}


Transacao::Transacao(std::time_t tempTimestamp, Cliente* tempCliente, Produtor* tempProdutor, map<Produto *, int> tempProdutos, float tempMontante, int tempIdEmpresaTransporte)
{
	data = tempTimestamp;
	comprador = tempCliente;
	vendedor = tempProdutor;
	produtos = tempProdutos;
	montante = tempMontante;

	ultimoId++;
	id = ultimoId;
}

Transacao::Transacao()
{
	ultimoId++;
	id = ultimoId;
}

bool Transacao::eliminarProdId(int id) {
	map<Produto *, int>::const_iterator it;
	for(it = produtos.begin(); it != produtos.end(); it++)
		if( (*it).first->getId() == id) {
			produtos.erase(it);
			return true;
		}

	return false;
}

ostream & operator<<(ostream &o, Transacao &t)
{
	stringstream ss;
	time_t time= t.getData();
	struct tm *timeinfo = localtime(&time);

	ss  << "Id : " << t.getId()
		<< " | Data : " << (timeinfo->tm_year+1900) <<"/"<< (timeinfo->tm_mon+1) <<"/"<<timeinfo->tm_mday
		<< " | Comprador : " << t.getClienteId()
		<< " | Vendedor : " << t.getVendedorId()
		<< " | Transporte : " << t.getEmpresaTransporteId();
	
	o << center(ss.str()) << endl;

	ss.str("");

	ss<< "Produtos: ";
	map<Produto *, int> produtos = t.getProdutos();
	for(map<Produto *, int>::iterator j=produtos.begin();j!=produtos.end();j++)
	{
		ss << " Id : " << (*j).first->getId() <<" Quantidade: "<<(*j).second << " |";
	}
	o << center(ss.str()) << endl;
	o << center("Montante Total: "+DoubleToString(t.getMontante()));

	return o;
}