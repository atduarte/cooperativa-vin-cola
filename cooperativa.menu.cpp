#include "cooperativa.h";

/** \brief Cabeçalho dos Menus
* 
*   Põe cabeçalho comum a todos os menus e apresenta o erro passado por referência.
*/
void Cooperativa::menu_cabecalho(string &erro) 
{
	cout << endl;

	stringCenter("Gestao de Cooperativa Vinicola (v1.0)");

	if(nome.size() == 0) 
		stringCenter("Desenvolvido por: Andre Duarte, Carlos Matias e Sergio Esteves");
	else 
		stringCenter(nome);

	cout << endl;

	if(erro.size() != 0) {
		stringCenter(erro);
		cout << endl;
	}

	erro = "";
}
void Cooperativa::menu_introducao()
{
	string erro, buffer;
	char op;

	do {
		system("cls");
		erro = "";

		menu_cabecalho(erro);

		stringCenter("F - Carregar Ficheiro ");
		stringCenter("N - Criar Novo        ");

		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'F')
			if( carregaFicheiro() ) {
				menu_principal();
				return;
			} else {
				erro = "Erro: Ficheiro nao foi carregado com sucesso";
			}
		else if(op == 'N')
			menu_criar();
		else {
			erro = "Erro: Opcao nao identificada";
		}

	} while(true);
}
void Cooperativa::menu_criar() {
	string erro, tempNome;
	bool repeat;

	do {
		system("cls");

		menu_cabecalho(erro);
		stringCenter("= Criar Cooperativa =");
		cout << endl;

		stringCenter("^ - Para voltar para tras.");
		cout << endl;

		stringCenter("Nome: ", false);
		getline(cin, tempNome);

		if(tempNome == "^") {
			menu_introducao();
		} else if(tempNome.size() > 0) {
			nome = tempNome;
			menu_principal();
			break;
		} else {
			erro = "Erro: Insira um nome valido.";
		}

	} while(true);
}
void Cooperativa::menu_principal(string mensagem) {
	string erro = mensagem, buffer;
	char op;

	do {
		system("cls");
		erro = "";

		menu_cabecalho(erro);

		stringCenter("= Menu Principal =");
		cout << endl;

		stringCenter("A - Adicionar");
		stringCenter("T - Alterar  ");
		stringCenter("L - Listar   ");
		stringCenter("E - Eliminar ");
		stringCenter("G - Gravar   ");
		stringCenter("S - Sair     ");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
			menu_adicionar();
		else if(op == 'T')
			menu_alterar();
		else if(op == 'L')
			menu_listar();
		else if(op == 'E')
			menu_eliminar();
		else if(op == 'G')
			menu_gravar();
		else if(op == 'S') {
			if( menu_sair() )
				break;
		} else {
			erro = "Erro: Opcao nao identificada.";
		}

	} while(true);

}


void Cooperativa::menu_adicionar() {
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar =");
		cout << endl;

		stringCenter("C - Cliente              ");
		stringCenter("E - Empresa de Transporte");
		stringCenter("P - Produto              ");
		stringCenter("R - Produtores           ");
		stringCenter("T - Transacao            ");
		stringCenter("U - Uva                  ");
		stringCenter("V - Viatura              ");
		cout << endl;
		stringCenter("M - Voltar ao Menu");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'C')
			menu_adicionarCliente();
		else if(op == 'E')
			menu_adicionarEmpresaTransporte();
		else if(op == 'P')
			menu_adicionarProduto();
		else if(op == 'R')
			menu_adicionarProdutor();
		else if(op == 'T') {
			menu_adicionarTransacao();
			actualizainativos();
		}
		else if(op == 'U')
			menu_adicionarUva();
		else if(op == 'V')
			menu_adicionarViatura();
		else if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";


	} while(true);
}
void Cooperativa::menu_alterar() {
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Alterar =");
		cout << endl;

		stringCenter("C - Cliente       ");
		stringCenter("P - Produto       ");
		stringCenter("R - Produtores    ");
		stringCenter("T - Transacao     ");
		stringCenter("U - Uva           ");
		cout << endl;
		stringCenter("M - Voltar ao Menu");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'C')
			menu_alterarCliente();
		else if(op == 'P')
			menu_alterarProduto();
		else if(op == 'R')
			menu_alterarProdutor();
		else if(op == 'T')
			menu_alterarTransacao();
		else if(op == 'U')
			menu_alterarUva();
		else if(op == '^' || op == 'M')
			break;
		else {
			erro = "Erro: Opcao nao identificada.";
		}

	} while(true);
}
void Cooperativa::menu_listar() {
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar =");
		cout << endl;

		stringCenter("C - Cliente              ");
		stringCenter("E - Empresa de Transporte");
		stringCenter("I - Clientes Inativos    ");
		stringCenter("P - Produto              ");
		stringCenter("R - Produtores           ");
		stringCenter("T - Transacao            ");
		stringCenter("U - Uva                  ");
		stringCenter("V - Viatura              ");
		cout << endl;
		stringCenter("M - Voltar ao Menu");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'C')
			menu_listarCliente();
		else if(op == 'E')
			menu_listarEmpresa();
		else if(op== 'I')
            menu_listarinativos();
		else if(op == 'P')
			menu_listarProduto();
		else if(op == 'R')
			menu_listarProdutor();
		else if(op == 'T')
			menu_listarTransacao();
		else if(op == 'U')
			menu_listarUva();
		else if(op == 'V')
			menu_listarViatura();
		else if(op == '^' || op == 'M') {
			break;
		} else
			erro = "Erro: Opcao nao identificada.";

	} while(true);
}
void Cooperativa::menu_eliminar() {
	string erro, buffer;
	char op;
	int id;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Eliminar =");
		cout << endl;

		stringCenter("C - Cliente              ");
		stringCenter("E - Empresa de Transporte");
		stringCenter("I - Cliente Inactivo     ");
		stringCenter("P - Produto              ");
		stringCenter("R - Produtor             ");
		stringCenter("T - Transacao            ");
		stringCenter("U - Uva                  ");
		stringCenter("V - Viatura              ");
		cout << endl;
		stringCenter("M - Voltar ao Menu");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'C' || op == 'P' || op == 'R' || op == 'T' || op == 'U' || op == 'I' || op == 'E' || op == 'V') {
			stringCenter("ID: ", false);
			getline(cin, buffer);
			if ( istringstream(buffer) >> id )  { //Converte string para numero

				if( elimina(op, id) ) {
					erro = "Eliminado com sucesso.";
					continue;
				} else {
					erro = "Erro: Impossivel eliminar elemento com esse Id";
					continue;
				}

			} else {
				erro = "Erro: ID invalido.";
			}
		} else if(op == '^' || op == 'M') {
			break;
		} else
			erro = "Erro: Opcao nao identificada.";

	} while(true);
}
void Cooperativa::menu_gravar() {
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Gravar =");
		cout << endl;

		stringCenter("Tem a certeza que pretende continuar?");
		stringCenter("(Y/N)");

		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'Y') {
			escreveFicheiro();
			break;
		} else if(op == 'N' || op == '^' || op == 'M')
			break;
		else {
			erro = "Erro: Opcao nao identificada.";
		}
	} while(true);
}
bool Cooperativa::menu_sair() {
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Sair =");
		cout << endl;

		stringCenter("Tem a certeza que pretende continuar?");
		stringCenter("(Y/N)");

		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'Y') {
			return true;
		} else if(op == 'N' || op == '^' || op == 'M')
			return false;
		else {
			erro = "Erro: Opcao nao identificada.";
		}
	} while(true);
}


void Cooperativa::menu_adicionarCliente()
{
	string erro, tempNome, tempContacto;
	int temp;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar Cliente =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		stringCenter("Nome: ", false);
		getline(cin, tempNome);

		if(tempNome == "^") {
			break;
		} else if(tempNome.size() > 0) {

			//Nome
			while(true) {
				stringCenter("Contacto (9 digitos): ", false);
				getline(cin, tempContacto);

				if(tempContacto == "^")
					return;
				else if(tempContacto.size() == 9 && (istringstream(tempContacto) >> temp))
					break; 
				else
					stringCenter("Erro: Insira um contacto valido.");
			}


			Cliente *p1 = new Cliente(tempNome, tempContacto);
			clientes.push_back(p1);
			clientesinativos.insert(*p1);

			stringstream ss;
			ss << "Sucesso: Cliente " << p1->getId() << " adicionado.";
			erro = ss.str();
		} else {
			erro = "Erro: Insira um nome valido.";
		}

	} while(true);
}
void Cooperativa::menu_adicionarProduto()
{
	string erro, buffer;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar Produto =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		string tempNome, tempTipo;

		//Nome
		while(true) {
			stringCenter("Nome: ", false);
			getline(cin, tempNome);

			if(tempNome == "^")
				return;
			else if(tempNome.size() > 0)
				break; 
			else
				stringCenter("Erro: Insira um nome valido.");
		}

		//Uvas
		map<Uva*, float> uvas;
		stringstream ss;
		vector<Uva*> tempUvas;
		while(true) {
			stringCenter("IDs das Uvas (separados por espaco): ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;

			tempUvas = procuraUvas(buffer, " ");

			if(tempUvas.size() > 0)
				break;
			else
				stringCenter("Erro: 0 uvas reconhecidas.");
		}

		//Quantidade de cada uva
		for(unsigned int i = 0; i < tempUvas.size(); i++)
		{
			float quantidade;
			ss.str(" "); //Limpar a stringstream
			ss << "Quantidade (em litros) de " << tempUvas[i]->getEspecie() << ": ";
			stringCenter( ss.str() , false);

			while(true) {
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if ( (istringstream(buffer) >> quantidade) && quantidade > 0)
					break;
				else {
					stringCenter("Erro: Quantidade Invalida.");
					continue;
				}
			}

			uvas[ tempUvas[i] ] = quantidade;
		}

		//Tipo
		while(true) {
			stringCenter("Tipo (Vinho, VinhoDoPorto, Espumante ou Derivado): ", false);
			getline(cin, tempTipo);

			if(tempTipo == "^")
				return;

			tempTipo[0] = toupper(tempTipo[0]);

			if(tempTipo == "Vinho" || tempTipo == "VinhoDoPorto" || tempTipo == "Espumante" || tempTipo == "Derivado")
				break;
			else if(tempTipo == "Vinho porto" || tempTipo == "Vinho do Porto" || tempTipo == "Vinho do porto" || tempTipo == "Porto" || tempTipo == "Vinhodoporto") {
				tempTipo = "VinhoDoPorto";
				break;
			} else
				stringCenter("Erro: Insira um tipo valido.");
		}

		//Cenas especificas

		//Subtipo - Vinho || VinhoDoPorto || Derivado
		string tempSubTipo;
		if (tempTipo == "Vinho" || tempTipo == "VinhoDoPorto" || tempTipo == "Derivado" )
		{
			while(true) {
				stringCenter("Subtipo: ", false);
				getline(cin, tempSubTipo);

				if(tempSubTipo == "^")
					return;
				else if(tempSubTipo.size() > 0)
					break; 
				else
					stringCenter("Erro: Insira um subtipo valido.");
			}
		}

		//Ano Colheita + Volume Alcoolico - Vinho || VinhoDoPorto || Espumante
		int tempAnoColheita;
		double tempVolumeAlcoolico ;
		if (tempTipo == "Vinho" || tempTipo == "VinhoDoPorto" || tempTipo == "Espumante" )
		{
			while(true) {
				stringCenter("Ano Colheita: ", false);
				getline(cin, buffer);

				time_t t = time(NULL);
				tm* timePtr = localtime(&t);

				if(buffer == "^")
					return;
				else if( (istringstream(buffer) >> tempAnoColheita) && tempAnoColheita > 0 && tempAnoColheita < (timePtr->tm_year + 1900))
					break; 
				else
					stringCenter("Erro: Insira um ano valido.");
			}

			while(true) {
				stringCenter("Volume Alcoolico: ", false);
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if( (istringstream(buffer) >> tempVolumeAlcoolico) && tempVolumeAlcoolico >= 0 && tempVolumeAlcoolico <= 100)
					break; 
				else
					stringCenter("Erro: Insira um volume valido.");
			}
		}

		//Tempo Armazenamento - VinhoDoPorto
		int tempTempoArmazenamento;
		if (tempTipo == "VinhoDoPorto")
		{
			while(true) {
				stringCenter("Tempo de Armazenamento: ", false);
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if( (istringstream(buffer) >> tempTempoArmazenamento) && tempTempoArmazenamento >= 0)
					break; 
				else
					stringCenter("Erro: Insira um tempo de armazenamento valido.");
			}
		}

		//VolumeCo2 + QualidadeCo2 - Espumante
		double tempVolCo2;
		string tempQualidadeCo2;
		if (tempTipo == "Espumante")
		{
			while(true) {
				stringCenter("Volume CO2: ", false);
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if( (istringstream(buffer) >> tempVolCo2) && tempVolCo2 > 0 && tempVolCo2 < 100)
					break; 
				else
					stringCenter("Erro: Insira um volume valido.");
			}


			while(true) {
				stringCenter("Qualidade CO2 (Boa/Media/Fraca): ", false);
				getline(cin, tempQualidadeCo2);

				if(tempQualidadeCo2 == "^")
					return;

				//Transformar boa, BOA, bOa, etc em Boa
				for(unsigned int i = 0; i < tempQualidadeCo2.size(); i++)
					if(i == 0)
						tempQualidadeCo2[0] = toupper(tempQualidadeCo2[0]);
					else
						tempQualidadeCo2[i] = tolower(tempQualidadeCo2[i]);

				if( tempQualidadeCo2 == "Boa" || tempQualidadeCo2 == "Media" || tempQualidadeCo2 == "Fraca" )
					break; 
				else
					stringCenter("Erro: Insira uma qualidade valida.");
			}
		}

		//Preco - Derivado
		float tempPreco;
		if (tempTipo == "Derivado")
		{
			while(true) {
				stringCenter("Preco: ", false);
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if( (istringstream(buffer) >> tempPreco) && tempPreco >= 0)
					break; 
				else
					stringCenter("Erro: Insira um preco valido.");
			}
		}

		//Produtor
		Produtor *pr;
		while(true) {
			string buffer;
			int idP;
			stringCenter("Produtor: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> idP) ) {
				try {
					pr = procuraProdutor(idP);
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

		//Criar objectos e adicionar aos vectores
		ss.str(" ");
		if (tempTipo == "Vinho") {
			Vinho *v1 = new Vinho(tempNome, uvas, tempSubTipo, tempAnoColheita, tempVolumeAlcoolico);
			addProduto(v1);
			pr->addProduto(v1);
			ss << "Sucesso: Vinho " << v1->getId() << " adicionado.";
		} else if (tempTipo == "VinhoDoPorto") {
			VinhoDoPorto *vp1 = new VinhoDoPorto(tempNome, uvas, tempSubTipo, tempAnoColheita, tempVolumeAlcoolico, tempTempoArmazenamento);
			addProduto(vp1);
			pr->addProduto(vp1);
			ss << "Sucesso: Vinho do Porto " << vp1->getId() << " adicionado.";
		} else if (tempTipo == "Espumante") {
			Espumante *e1 = new Espumante(tempNome, uvas, tempAnoColheita, tempVolumeAlcoolico, tempVolCo2, tempQualidadeCo2);
			addProduto(e1);
			pr->addProduto(e1);
			ss << "Sucesso: Espumante " << e1->getId() << " adicionado.";
		} else if (tempTipo == "Derivado") {
			Derivado *d = new Derivado(tempNome, uvas, tempSubTipo, tempPreco);
			addProduto(d);
			pr->addProduto(d);
			ss << "Sucesso: Derivado " << d->getId() << " adicionado.";
		} else {
			erro = "Erro: Tente Novamente";
		}

		erro = ss.str();

	} while(true);
}
void Cooperativa::menu_adicionarProdutor()
{
	string erro, tempNome, tempProdutosIds, tempUvasIds, mensagem;
	vector<Produto*> tempProdutos;
	vector<Uva*> tempUvas;
	std::stringstream ss;

	do {
		system("cls");

		//Cabeçalho
		menu_cabecalho(erro);

		stringCenter("= Adicionar Produtor =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		//Nome
		stringCenter("Nome: ", false);
		getline(cin, tempNome);

		if(tempNome == "^") {
			return;
		} else if(tempNome.size() == 0) {
			erro = "Erro: Insira um nome valido.";
			continue;
		}
		cout << endl;

		//Produtos
		stringCenter("Id dos Produtos (separados por espacos): ");
		stringCenter(" ", false);
		getline(cin, tempProdutosIds);

		if(tempProdutosIds == "^"){
			return;
		} else if(tempProdutosIds.size() > 0)
			tempProdutos = procuraProdutos( tempProdutosIds , " ");

		ss.str( "" );
		ss << "Foram reconhecidos " << tempProdutos.size() << " produtos.";
		stringCenter(ss.str()); 
		cout << endl;

		//Uvas
		stringCenter("Id das Uvas (separados por espacos): ");
		stringCenter(" ", false);
		getline(cin, tempUvasIds);

		if(tempUvasIds == "^") {
			return;
		} else if(tempUvasIds.size() > 0)
			tempUvas = procuraUvas( tempUvasIds , " ");

		ss.str( "" );
		ss << "Foram reconhecidas " << tempUvas.size() << " uvas.";
		stringCenter(ss.str()); 

		//Adicionar...
		Produtor *p = new Produtor(tempNome, tempProdutos, tempUvas);
		addProdutor(p);

		ss.str(" ");
		ss << "Sucesso: Produtor " << p->getId() << " adicionado.";
		erro = ss.str();

		cin.clear();

	} while(true);

}
void Cooperativa::menu_adicionarTransacao()
{
	string erro, buffer;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar Transacao =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		//Data
		struct tm tm;
		time_t t;
		while(true) {
			stringCenter("Data (forma: 23:05:45 5/12/1993) (0 = actual): ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if (buffer == "0") {
				t = time(NULL);
				break;
			}

			if( sscanf (buffer.c_str(),"%i:%i:%i %i/%i/%i", &tm.tm_hour, &tm.tm_min, &tm.tm_sec, &tm.tm_mday, &tm.tm_mon, &tm.tm_year) != 6 ) {
				stringCenter("Erro: Data invalida.");
				continue;
			}


			tm.tm_mon--;
			if(tm.tm_year > 1900) tm.tm_year -= 1900;

			t = mktime(&tm);

			if(t == -1) {
				stringCenter("Erro: Data invalida.");
				continue;
			}

			break;
		}

		//Produtos
		vector<Produto*> tempProdutos;
		map<Produto*, int> produtos;
		stringstream ss;

		stringCenter("IDs dos Produtos (separados por virgula): ", false);
		getline(cin, buffer);

		if(buffer == "^")
			goto end;

		tempProdutos = procuraProdutos(buffer);

		ss << tempProdutos.size() << " produtos identificados.";
		stringCenter(ss.str());

		for(unsigned int i = 0; i < tempProdutos.size(); i++)
		{
			int quantidade;
			ss.str(" "); //Limpar a stringstream
			ss << "Quantidade de " << tempProdutos[i]->getNome() << ": ";
			stringCenter( ss.str() , false);

			while(true) {
				getline(cin, buffer);

				if(buffer == "^")
					goto end;
				else if ( istringstream(buffer) >> quantidade )
					break;
				else {
					stringCenter("Erro: Quantidade Invalida.");
					continue;
				}
			}
			produtos[ tempProdutos[i] ] = quantidade;
		}

		//Comprador
		Cliente* comprador;
		while(true)
		{
			int id;
			stringCenter("Id do Comprador: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				goto end;
			else if ( istringstream(buffer) >> id ) {
				try {
					comprador = procuraCliente(id);
					break;
				} catch(Erro) {	}
			} 

			stringCenter("Erro: Id de Cliente Invalido.");

		}

		//Vendedor
		Produtor* vendedor;
		while(true)
		{
			int id;
			stringCenter("Id do Vendedor: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				goto end;
			else if ( istringstream(buffer) >> id ) {
				try {
					vendedor = procuraProdutor(id);
					break;
				} catch(Erro) {	}
			} 

			stringCenter("Erro: Id de Produtor Invalido.");

		}

		//Montante
		float montante = 0;
		for(map<Produto *, int>::const_iterator it = produtos.begin(); it != produtos.end(); it++)
		{
			montante += (*it).first->calculaPreco() * (*it).second;
		}


		Transacao *t1 = new Transacao(t,comprador,vendedor,produtos,montante);
		addTransacao(t1);

		ss.str(" ");
		ss << "Sucesso: Transacao " << t1->getId() << " adicionada.";
		erro = ss.str();

	} while(true);

end: return;

}
void Cooperativa::menu_adicionarUva()
{
	string erro;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar Uva =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		string aroma, casta, cor, especie, regiao;
		float preco;

		//Aroma
		while(true) {
			string buffer;
			stringCenter("Aroma: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if(buffer.size() > 0) {
				aroma = buffer;
				break;
			} else
				stringCenter("Erro: Aroma invalida");
		}

		//Casta
		while(true) {
			string buffer;
			stringCenter("Casta: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if(buffer.size() > 0) {
				casta = buffer;
				break;
			} else
				stringCenter("Erro: Casta invalida");
		}

		//Cor
		while(true) {
			string buffer;
			stringCenter("Cor: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if(buffer.size() > 0) {
				cor = buffer;
				break;
			} else
				stringCenter("Erro: Cor invalida");
		}

		//Especie
		while(true) {
			string buffer;
			stringCenter("Especie: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if(buffer.size() > 0) {
				especie = buffer;
				break;
			} else
				stringCenter("Erro: Especie invalida");
		}

		//Regiao
		while(true) {
			string buffer;
			stringCenter("Regiao: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if(buffer.size() > 0) {
				regiao = buffer;
				break;
			} else
				stringCenter("Erro: Regiao invalida");
		}

		//Preco
		while(true) {
			string buffer;
			stringCenter("Preco: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> preco) && preco > 0) {
				break;
			} else
				stringCenter("Erro: Preco invalido");
		}

		//Produtor
		Produtor *pr;
		while(true) {
			string buffer;
			int idP;
			stringCenter("Produtor: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> idP) ) {
				try {
					pr = procuraProdutor(idP);
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

		Uva* u = new Uva(casta, especie, regiao, cor, aroma, preco);
		addUva(u);

		pr->addUva(u);

		stringstream ss;
		ss << "Sucesso: Uva " << u->getId() << " adicionada.";
		erro = ss.str();;

	} while(true);
}
void Cooperativa::menu_adicionarViatura()
{
	string erro;
	int idEmpresa;
	double tempCapacidade;
	int temp;

	if( empresasTransporte.size() == 0)
	{	
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar Viatura =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		stringCenter("Não é possível adicionar viaturas sem uma empresa para as atribuir.");
		
		system("pause");

		return;
	}

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar Viatura =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		while(true) {
			string buffer;
			stringCenter("Capacidade: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> tempCapacidade) && tempCapacidade > 0) {
				break;
			} else
				stringCenter("Erro: Capacidade invalida");
		}
		
		Viatura v1 (tempCapacidade);
		
		stringCenter("= Insira o Id da Empresa =");
		cout << endl;

		while(true) {
			string buffer;
			stringCenter("Id da Empresa: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> idEmpresa) && procuraEmpresaTransporte(idEmpresa).getNome().size() > 0) {
				break;
			} else
				stringCenter("Erro: Id invalido");
		}

		EmpresaTransporte e1;
		e1 = procuraEmpresaTransporte(idEmpresa);

		eliminaEmpresaTransporte(e1.getId());
		e1.adicionaViatura(v1);	
		empresasTransporte.push(e1);
		
		stringstream ss;
		ss << "Sucesso: Viatura " << v1.getId() << " adicionada na Empresa " << e1.getId() << ".";
		erro = ss.str();
		

	} while(true);
}
void Cooperativa::menu_adicionarEmpresaTransporte()
{
	string erro, tempNome;
	double tempDistancia;
	int temp;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Adicionar Empresa =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		stringCenter("Nome: ", false);
		getline(cin, tempNome);
		cout << endl;

		if(tempNome == "^") {
			break;
		} else if(tempNome.size() > 0) {

			while(true) {
				string buffer;
				stringCenter("Distancia: ", false);
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if( (istringstream(buffer) >> tempDistancia) && tempDistancia > 0) {
					break;
				} else
					stringCenter("Erro: Distancia invalida");
			}

			list<Viatura> v;

			EmpresaTransporte e1(tempNome, tempDistancia, v);
			empresasTransporte.push(e1);

			stringstream ss;
			ss << "Sucesso: Empresa " << e1.getId() << " adicionada.";
			erro = ss.str();
		} else {
			erro = "Erro: Insira um nome valido.";
		}

	} while(true);

}

void Cooperativa::menu_alterarCliente() 
{
	string erro, buffer;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Alterar Cliente =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		//Id
		int id;
		Cliente *c;
		while(true) {
			stringCenter("Id: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> id) ) {
				try {
					c = procuraCliente(id);
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

		//Nome
		while(true) {
			stringCenter("Novo nome: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( buffer.size() > 0 ) {
				c->setNome(buffer);
				erro = "Sucesso: Cliente modificado.";
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

	} while(true);
}
void Cooperativa::menu_alterarProduto() 
{
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Alterar Produto =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		//Id
		int id;
		Produto *p;
		while(true) {
			stringCenter("Id: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> id) ) {
				try {
					p = procuraProduto(id);
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

		//
		//

		while(true)
		{
			system("cls");

			menu_cabecalho(erro);

			stringstream ss;
			ss << "= Alterar Produto - " << p->getNome() << " =";
			stringCenter( ss.str() );
			cout << endl;

			stringCenter("^ - Voltar Atras");
			cout << endl;

			//Apresentacao das hipoteses
			stringCenter("N - Nome                  ");
			if(p->getTipo() == "Vinho" || p->getTipo() == "VinhoDoPorto" ||  p->getTipo() == "Derivado")
				stringCenter("S - Subtipo               ");
			if(p->getTipo() == "Vinho" || p->getTipo() == "VinhoDoPorto" ||  p->getTipo() == "Espumante") {
				stringCenter("A - Ano Colheita          ");
				stringCenter("V - Volume Alcoolico      ");
			}
			if(p->getTipo() == "VinhoDoPorto")
				stringCenter("T - Tempo de Armazenamento");
			if(p->getTipo() == "Espumante") {
				stringCenter("C - Volume CO2            ");
				stringCenter("Q - Qualidade CO2         ");
			}
				stringCenter("P - Preco                 ");


			stringCenter("Opcao: ", false);
			getline(cin, buffer);

			if(buffer.size() == 1)
				op = toupper(buffer[0]);
			else
				op = ' ';

			//Nome
			if(op == 'N') {
				while(true) {
					stringCenter("Novo nome: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						p->setNome(buffer);
						erro = "Sucesso: Produto modificado.";
						break;
					} else
						stringCenter("Erro: Insira um nome valido.");
				}
				//Subtipo
			} else if(op == 'S' && (p->getTipo() == "Vinho" || p->getTipo() == "VinhoDoPorto" ||  p->getTipo() == "Derivado")) {
				while(true) {
					stringCenter("Novo subtipo: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						p->setSubTipo(buffer);
						erro = "Sucesso: Produto modificado.";
						break;
					} else
						stringCenter("Erro: Insira um nome valido.");
				}
				//Ano Colheita
			} else if(op == 'A' && (p->getTipo() == "Vinho" || p->getTipo() == "VinhoDoPorto" ||  p->getTipo() == "Espumante")) {
				while(true) {
					int tempAnoColheita;
					stringCenter("Ano Colheita: ", false);
					getline(cin, buffer);

					time_t t = time(NULL);
					tm* timePtr = localtime(&t);

					if(buffer == "^")
						break;
					else if( (istringstream(buffer) >> tempAnoColheita) && tempAnoColheita > 0 && tempAnoColheita < (timePtr->tm_year + 1900)) {
						p->setAnoColheita(tempAnoColheita);
						erro = "Sucesso: Produto modificado.";
						break; 
					} else
						stringCenter("Erro: Insira um ano valido.");
				}
				//Volume Alcoolico
			} else if(op == 'V' && (p->getTipo() == "Vinho" || p->getTipo() == "VinhoDoPorto" ||  p->getTipo() == "Espumante")) {
				while(true) {
					double tempVolumeAlcoolico;
					stringCenter("Volume Alcoolico: ", false);
					getline(cin, buffer);
					if(buffer == "^")
						break;
					else if( (istringstream(buffer) >> tempVolumeAlcoolico) && tempVolumeAlcoolico >= 0 && tempVolumeAlcoolico <= 100) {
						p->setVolumeAlcoolico(tempVolumeAlcoolico);
						erro = "Sucesso: Produto modificado.";
						break; 
					} else
						stringCenter("Erro: Insira um volume valido.");
				}
				//Tempo Armazenamento
			} else if(op == 'T' && (p->getTipo() == "VinhoDoPorto")) {
				while(true) {
					int tempTempoArmazenamento;
					stringCenter("Tempo de Armazenamento: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( (istringstream(buffer) >> tempTempoArmazenamento) && tempTempoArmazenamento >= 0){
						p->setTempoArmazenamento(tempTempoArmazenamento);
						erro = "Sucesso: Produto modificado.";
						break; 
					} else
						stringCenter("Erro: Insira um tempo de armazenamento valido.");
				}
				//Volume CO2
			} else if(op == 'C' && (p->getTipo() == "Espumante")) {
				while(true) {
					double tempVolCo2;
					stringCenter("Volume CO2: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( (istringstream(buffer) >> tempVolCo2) && tempVolCo2 >= 0 && tempVolCo2 <= 100) {
						p->setVolumeCo2(tempVolCo2);
						erro = "Sucesso: Produto modificado.";
						break; 
					} else
						stringCenter("Erro: Insira um volume valido.");
				}
				//Qualidade CO2
			} else if(op == 'Q' && (p->getTipo() == "Espumante")) {
				while(true) {
					stringCenter("Qualidade CO2 (Boa/Media/Fraca): ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;

					//Transformar boa, BOA, bOa, etc em Boa
					for(unsigned int i = 0; i < buffer.size(); i++)
						if(i == 0)
							buffer[0] = toupper(buffer[0]);
						else
							buffer[i] = tolower(buffer[i]);

					if( buffer == "Boa" || buffer == "Media" || buffer == "Fraca" ) {
						p->setQualidadeCo2(buffer);
						erro = "Sucesso: Produto modificado.";
						break; 
					} else
						stringCenter("Erro: Insira uma qualidade valida.");
				}
				//Preco
			} else if(op == 'P') {
				while(true) {
					float preco;
					stringCenter("Preco: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( (istringstream(buffer) >> preco) && preco >= 0) {
						p->setPreco(preco);
						erro = "Sucesso: Produto modificado.";
						break; 
					} else
						stringCenter("Erro: Insira um volume valido.");
				}
			} else if(op == '^')
				break;
			else 
				stringCenter("Erro: Opcao nao identificada.");

			cout << endl;

		}

perguntarId: continue;
		//
		//

	} while(true);
}
void Cooperativa::menu_alterarProdutor() 
{
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Alterar Produtor =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		//Id
		int id;
		Produtor *pr;
		while(true) {
			stringCenter("Id: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> id) ) {
				try {
					pr = procuraProdutor(id);
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

		//Opcoes
		while(true) {
			system("cls");

			menu_cabecalho(erro);

			stringstream ss;
			ss << "= Alterar Produtor " << pr->getNome() << " =";
			stringCenter( ss.str() );
			cout << endl;

			stringCenter("^ - Voltar Atras");
			cout << endl;

			stringCenter("N - Nome            ");
			stringCenter("A - Atribuir Produto");
			stringCenter("T - Atribuir Uva    ");
			stringCenter("P - Eliminar Produto");
			stringCenter("U - Eliminar Uva    ");
			cout << endl;

			stringCenter("Opcao: ", false);
			getline(cin, buffer);

			if(buffer.size() == 1)
				op = toupper(buffer[0]);
			else
				op = ' ';

			//Nome
			if(op == 'N') {
				while(true) {
					stringCenter("Novo nome: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						pr->setNome(buffer);
						erro = "Sucesso: Produtor modificado.";
						break;
					} else
						stringCenter("Erro: Insira um nome valido.");
				}
				//Atribuir Produto
			} else if(op == 'A') {
				Produto *p;
				while(true) {
					int idProd;
					stringCenter("Id do Produto: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idProd) ) {
						try {
							p = procuraProduto(idProd);
						} catch(Erro) {
							stringCenter("Erro: Insira um id valido.");
							continue;
						}
						if(!pr->addProduto(p)) {
							stringCenter("Erro: Produto duplicado ou Produtor nao possui as uvas necessarias.");
							continue;
						}
						erro = "Sucesso: Produtor modificado.";
						break;
					} else
						stringCenter("Erro: Insira um id valido.");
				}
			} else if(op == 'T') {
				Uva *u;
				while(true) {
					int idUva;
					stringCenter("Id da Uva: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idUva) ) {
						try {
							u = procuraUva(idUva);
						} catch(Erro) {
							stringCenter("Erro: Insira um id valido.");
							continue;
						}
						pr->addUva(u);
						erro = "Sucesso: Produtor modificado.";
						break;
					} else
						stringCenter("Erro: Insira um id valido.");
				}
			} else if(op == 'P') {
				Produto *p;
				while(true) {
					int idProd;
					stringCenter("Id do Produto: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idProd) && pr->eliminarProdId(idProd) ) {
						erro = "Sucesso: Produto Eliminado.";
						break;
					} else
						stringCenter("Erro: Insira um id valido.");
				}
			} else if(op == 'U') {
				Uva *u;
				while(true) {
					int idUva;
					stringCenter("Id da Uva: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idUva) && pr->eliminarUvaId(idUva) ) {
						erro = "Sucesso: Uva Eliminada.";
						break;
					} else
						stringCenter("Erro: Insira um id valido.");
				}
			} else if(op == '^')
				break;
			else
				erro = "Erro: Opcao nao identificada";
		}	

	} while(true);
}
void Cooperativa::menu_alterarTransacao()
{
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Alterar Transacao =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		//Id
		int id;
		Transacao *t;
		while(true) {
			stringCenter("Id: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> id) ) {
				try {
					t = procuraTransacao(id);
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

		//Opcoes
		while(true) {
			system("cls");

			menu_cabecalho(erro);

			stringstream ss;
			ss << "= Alterar Transacao " << t->getId() << " =";
			stringCenter( ss.str() );
			cout << endl;

			stringCenter("^ - Voltar Atras");
			cout << endl;

			stringCenter("D - Data                     ");
			stringCenter("A - Atribuir/Alterar Produto ");
			stringCenter("E - Eliminar Produto         ");
			stringCenter("C - Alterar Comprador        ");
			stringCenter("V - Alterar Vendedor         ");
			cout << endl;

			stringCenter("Opcao: ", false);
			getline(cin, buffer);

			if(buffer.size() == 1)
				op = toupper(buffer[0]);
			else
				op = ' ';

			//Data
			if(op == 'D') {
				struct tm tm;
				time_t t1;
				while(true) {
					stringCenter("Data (forma: 23:05:45 5/12/1993) (0 = actual): ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if (buffer == "0") {
						t1 = time(NULL);
						t->setData(t1);
						erro = "Sucesso: Transacao modificada.";
						break;
					}


					if( sscanf (buffer.c_str(),"%i:%i:%i %i/%i/%i", &tm.tm_hour, &tm.tm_min, &tm.tm_sec, &tm.tm_mday, &tm.tm_mon, &tm.tm_year) != 6 ) {
						stringCenter("Erro: Data invalida.");
						continue;
					}

					tm.tm_mon--;
					if(tm.tm_year > 1900) tm.tm_year -= 1900;

					t1 = mktime(&tm);

					if(t1 == -1) {
						stringCenter("Erro: Data invalida.");
						continue;
					} else {
						t->setData(t1);
						erro = "Sucesso: Transacao modificada.";
						break;
					}
				}
				//Atribuir Produto
			} else if(op == 'A') {
				Produto* produto;
				int idP;
				while(true) {
					stringCenter("Id do Produto: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idP) ) {
						try {
							produto = procuraProduto(idP);
						} catch(Erro) {
							stringCenter("Erro: Id Invalido.");
							continue;
						}
						break;
					} else
						stringCenter("Erro: Id Invalido.");
				}

				int quantidade;
				while(true) {
					stringCenter("Quantidade: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> quantidade) && quantidade >= 0 ) {
						t->addProduto(produto, quantidade);
						erro = "Sucesso: Transacao modificada.";
						break;
					} else
						stringCenter("Erro: Quantidade invalida.");
				}
			} else if(op == 'E') {
				int idP;
				while(true) {
					stringCenter("Id do Produto a Remover: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idP) && t->eliminarProdId(idP) ) {
						erro = "Sucesso: Transacao modificada.";
						break;
					} else
						stringCenter("Erro: Id invalido.");
				}
			} else if(op == 'C') {
				Cliente* comprador;
				int idC;
				while(true) {
					stringCenter("Id do Comprador: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idC) ) {
						try {
							comprador = procuraCliente(idC);
						} catch(Erro) {
							stringCenter("Erro: Id Invalido.");
							continue;
						}
						t->setComprador(comprador);
						erro = "Sucesso: Transacao modificada.";
						break;
					} else
						stringCenter("Erro: Id Invalido.");
				}
			} else if(op == 'V') {
				Produtor* vendedor;
				int idP;
				while(true) {
					stringCenter("Id do Vendedor: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						return;
					else if( (istringstream(buffer) >> idP) ) {
						try {
							vendedor = procuraProdutor(idP);
						} catch(Erro) {
							stringCenter("Erro: Id Invalido.");
							continue;
						}
						t->setVendedor(vendedor);
						erro = "Sucesso: Transacao modificada.";
						break;
					} else
						stringCenter("Erro: Id Invalido.");
				}
			} else if(op == '^')
				break;
			else
				erro = "Erro: Opcao nao identificada";
		}

	} while(true);
}
void Cooperativa::menu_alterarUva()
{
	string erro, buffer;
	char op;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Alterar Uva =");
		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		//Id
		int id;
		Uva *u;
		while(true) {
			stringCenter("Id: ", false);
			getline(cin, buffer);

			if(buffer == "^")
				return;
			else if( (istringstream(buffer) >> id) ) {
				try {
					u = procuraUva(id);
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
				break;
			} else
				stringCenter("Erro: Insira um id valido.");
		}

		//Opcoes
		while(true) {
			system("cls");

			menu_cabecalho(erro);

			stringstream ss;
			ss << "= Alterar Uva " << u->getId() << " =";
			stringCenter( ss.str() );
			cout << endl;


			stringCenter("^ - Voltar Atras");
			cout << endl;

			stringCenter("C - Cor    ");
			stringCenter("A - Casta  ");
			stringCenter("E - Especie");
			stringCenter("R - Regiao ");
			stringCenter("O - Aroma  ");
			stringCenter("P - Preco  ");
			cout << endl;

			stringCenter("Opcao: ", false);
			getline(cin, buffer);

			if(buffer.size() == 1)
				op = toupper(buffer[0]);
			else
				op = ' ';

			//Nome
			if(op == 'C') {
				while(true) {
					stringCenter("Cor: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						u->setCor(buffer);
						erro = "Sucesso: Uva modificado.";
						break;
					} else
						stringCenter("Erro: Insira uma cor valida.");
				}
			} else if(op == 'A') {
				while(true) {
					stringCenter("Casta: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						u->setCasta(buffer);
						erro = "Sucesso: Uva modificado.";
						break;
					} else
						stringCenter("Erro: Insira uma casta valida.");
				}
			} else if(op == 'E') {
				while(true) {
					stringCenter("Especie: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						u->setEspecie(buffer);
						erro = "Sucesso: Uva modificado.";
						break;
					} else
						stringCenter("Erro: Insira uma especie valida.");
				}
			} else if(op == 'R') {
				while(true) {
					stringCenter("Regiao: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						u->setRegiao(buffer);
						erro = "Sucesso: Uva modificado.";
						break;
					} else
						stringCenter("Erro: Insira uma regiao valida.");
				}
			} else if(op == 'O') {
				while(true) {
					stringCenter("Aroma: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( buffer.size() > 0 ) {
						u->setAroma(buffer);
						erro = "Sucesso: Uva modificado.";
						break;
					} else
						stringCenter("Erro: Insira um aroma valido.");
				}
			} else if(op == 'P') {
				while(true) {
					float preco;
					stringCenter("Aroma: ", false);
					getline(cin, buffer);

					if(buffer == "^")
						break;
					else if( (istringstream(buffer) >> preco) ) {
						u->setPreco(preco);
						erro = "Sucesso: Uva modificado.";
						break;
					} else
						stringCenter("Erro: Insira um preco valido.");
				}
			} else if(op == '^')
				break;
			else
				erro = "Erro: Opcao nao identificada.";
		}

	} while(true);

}

void Cooperativa::menu_listarCliente() 
{
	char op;
	int n1,n2;
	string nome;
	string erro;
	string buffer,buffer2;
	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar Cliente =");
		cout << endl;

		stringCenter("A - Todos               ");
		stringCenter("B - Entre codigos       ");
		stringCenter("C - Por Nome            ");

		cout << endl;
		stringCenter("^ - Voltar Atras");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
		{
			menu_listarClientesTodos();
			cin.get();
			cin.clear();
		}
		else if(op == 'B')
		{
			stringCenter("ID1: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				stringCenter("ID2: ", false);
				getline(cin, buffer2);
				if ( istringstream(buffer2) >> n2 )
				{
					menu_listarClientesEntreId(n1,n2);
				}
				else
				{
					erro= "Erro Id invalido";
				}
			} else {
				erro = "Erro: ID invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == 'C')
		{
			stringCenter("Digite o Nome: ",false);
			getline(cin,nome);
			menu_listarClientesNome(nome);
			cin.get();
			cin.clear();
		}


		else if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";


	} while(true);

}
void Cooperativa::menu_listarProduto() 
{
	char op;
	string buffer,buffer2;
	int n1,n2;
	string nome;
	string erro;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar Produto =");
		cout << endl;

		stringCenter("A - Todos               ");
		stringCenter("B - Entre codigos       ");
		stringCenter("C - Por Nome            ");
		stringCenter("D - Por Uva(s)          ");
		stringCenter("E - Produtor            ");
		stringCenter("F - Tipo                ");

		cout << endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
		{
			menu_listarProdutosTodos();
			cin.get();
			cin.clear();
		}
		else if(op == 'B')
		{
			stringCenter("ID1: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				stringCenter("ID2: ", false);
				getline(cin, buffer2);
				if ( istringstream(buffer2) >> n2 )
				{
					menu_listarProdutosEntreId(n1,n2);
				}
				else
				{
					erro= "Erro Id invalido";
				}
			} else {
				erro = "Erro: ID invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == 'C')
		{
			stringCenter("Digite o Nome: ",false);
			getline(cin,nome);
			menu_listarProdutosNome(nome);
			cin.get();
			cin.clear();
		}
		else if(op == 'D')
		{
			string stringtemp;
			stringCenter("Digite as Uvas (separadas por espaco): ",false);
			getline(cin,stringtemp);
			menu_listarProdutosUvas(procuraUvas(stringtemp));
			cin.get();
			cin.clear();
		}
		else if(op == 'E')
		{
			string stringtemp;
			stringCenter("Digite o Id do Produtor: ",false);
			getline(cin,buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero

				menu_listarProdutosProdutor(n1);
			} else {
				erro = "Erro: ID invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == 'F')
		{
			stringCenter("Digite o Tipo de Produto: ",false);
			getline(cin,nome);
			menu_listarProdutosTipo(nome);
			cin.get();
			cin.clear();
		}






		else if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";




		//Perguntar todos, entre ids,nome, uva(s), produtor, tipos...

	} while(true);
}
void Cooperativa::menu_listarProdutor() 
{
	char op;
	string buffer,buffer2;
	int n1,n2;
	string nome;
	string erro;


	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar Produtor =");
		cout << endl;

		stringCenter("A - Todos               ");
		stringCenter("B - Entre codigos       ");
		stringCenter("C - Por Nome            ");
		stringCenter("D - Por Produto(s)      ");
		stringCenter("E - Por Uva(s)          ");

		cout<<endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;

		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
		{
			menu_listarProdutorTodos();
			cin.get();
			cin.clear();
		}
		else if(op == 'B')
		{
			stringCenter("ID1: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				stringCenter("ID2: ", false);
				getline(cin, buffer2);
				if ( istringstream(buffer2) >> n2 )
				{
					menu_listarProdutorEntreId(n1,n2);
				}
				else
				{
					erro= "Erro Id invalido";
				}
			} else {
				erro = "Erro: Id invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == 'C')
		{
			stringCenter("Digite o Nome: ",false);
			getline(cin,nome);
			menu_listarProdutorNome(nome);
			cin.get();
			cin.clear();
		}
		else if(op == 'D')
		{
			string stringtemp;
			stringCenter("Digite os Produtos (separadas por espaco): ",false);
			getline(cin,stringtemp);
			menu_listarProdutorProdutos(procuraProdutos(stringtemp," "));
			cin.get();
			cin.clear();
		}
		else if(op == 'E')
		{
			string stringtemp;
			stringCenter("Digite as Uvas (separadas por espaco): ",false);
			getline(cin,stringtemp);
			menu_listarProdutorUvas(procuraUvas(stringtemp));
			cin.get();
			cin.clear();
		}



		else if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";





		//Perguntar todos, entre ids, pelo nome, produto(s), uva(s)

	} while(true);

}
void Cooperativa::menu_listarTransacao()
{

	char op;
	string buffer,buffer2;
	int n1,n2;
	string nome;
	string erro;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar Transacao =");
		cout << endl;

		stringCenter("A - Todos               ");
		stringCenter("B - Entre codigos       ");
		stringCenter("C - Entre datas         ");
		stringCenter("D - Por Produto(s)      ");
		stringCenter("E - Por Comprador       ");
		stringCenter("F - Por Vendedor        ");

		cout<<endl;

		stringCenter("^ - Voltar Atras");
		cout << endl;



		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
		{
			menu_listarTransacaoTodos();
			cin.get();
			cin.clear();
		}
		else if(op == 'B')
		{
			stringCenter("ID1: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				stringCenter("ID2: ", false);
				getline(cin, buffer2);
				if ( istringstream(buffer2) >> n2 )
				{
					menu_listarTransacaoEntreId(n1,n2);
				}
				else
				{
					erro= "Erro Id invalido";
				}
			} else {
				erro = "Erro: Id invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == 'C')
		{
			struct tm tm;
			time_t t;
			struct tm tm2;
			time_t t2;
			while(true) {
				stringCenter("Data1 (forma: 23:05:45 5/12/1993) (0 = actual): ", false);
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if (buffer == "0") {
					t = time(NULL);
					break;
				}

				if( sscanf (buffer.c_str(),"%i:%i:%i %i/%i/%i", &tm.tm_hour, &tm.tm_min, &tm.tm_sec, &tm.tm_mday, &tm.tm_mon, &tm.tm_year) != 6 ) {
					stringCenter("Erro: Data invalida.");
					continue;
				}


				tm.tm_mon--;
				if(tm.tm_year > 1900) tm.tm_year -= 1900;

				t = mktime(&tm);

				if(t == -1) {
					stringCenter("Erro: Data invalida.");
					continue;
				}

				break;
			}
			while(true) {
				stringCenter("Data2 (forma: 23:05:45 5/12/1993) (0 = actual): ", false);
				getline(cin, buffer);

				if(buffer == "^")
					return;
				else if (buffer == "0") {
					t2 = time(NULL);
					break;
				}

				if( sscanf (buffer.c_str(),"%i:%i:%i %i/%i/%i", &tm2.tm_hour, &tm2.tm_min, &tm2.tm_sec, &tm2.tm_mday, &tm2.tm_mon, &tm2.tm_year) != 6 ) {
					stringCenter("Erro: Data invalida.");
					continue;
				}


				tm.tm_mon--;
				if(tm.tm_year > 1900) tm.tm_year -= 1900;

				t2 = mktime(&tm);

				if(t2 == -1) {
					stringCenter("Erro: Data invalida.");
					continue;
				}

				break;
			}

			menu_listarTransacaoEntreData(t,t2);
			cin.get();
			cin.clear();
		}
		else if(op == 'D')
		{
			string stringtemp;
			stringCenter("Digite os Produtos (separadas por espaco): ",false);
			getline(cin,stringtemp);
			menu_listarTransacaoProdutos(procuraProdutos(stringtemp));
			cin.get();
			cin.clear();
		}
		else if(op == 'E')
		{
			string stringtemp;
			stringCenter("Digite o id do Comprador: ",false);
			getline(cin,stringtemp);
			if ( (istringstream(stringtemp) >> n1) )  
			{
				menu_listarTransacaoComprador(n1);
			} else {
				erro = "Erro: Id invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == 'F')
		{
			string stringtemp;
			stringCenter("Digite o id do Vendedor: ",false);
			getline(cin,stringtemp);
			if ( istringstream(stringtemp) >> n1 )  
			{
				menu_listarTransacaoVendedor(n1);
			} else {
				erro = "Erro: Id invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";






		//Perguntar todos, entre ids, entre datas, produto(s), comprador, vendedor

	} while(true);
}
void Cooperativa::menu_listarUva()
{
	char op;
	string buffer,buffer2;
	int n1,n2;
	string nome;
	string erro;

	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar Uva =");
		cout << endl;

		stringCenter("A - Todos               ");
		stringCenter("B - Entre codigos       ");
		stringCenter("C - Por Produtor        ");
		stringCenter("D - Por Casta           ");
		stringCenter("E - Por Especie         ");
		stringCenter("F - Por Cor             ");
		stringCenter("G - Por Aroma           ");
		stringCenter("H - Por Regiao          ");
		stringCenter("I - Por Preco           ");

		cout<<endl;


		stringCenter("^ - Voltar Atras");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);

		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
		{
			menu_listarUvasTodos();
			cin.get();
			cin.clear();
		}
		if(op == 'B')
		{
			stringCenter("ID1: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				stringCenter("ID2: ", false);
				getline(cin, buffer2);
				if ( istringstream(buffer2) >> n2 )
				{
					menu_listarUvasEntreId(n1,n2);
					cin.get();
					cin.clear();
				}
				else
				{
					erro= "Erro Id invalido";
				}
			} else {
				erro = "Erro: ID invalido.";
			}
		}
		else if(op =='C')
		{
			stringCenter("ID: ", false);
			getline(cin, buffer);
			int id;

			if( (istringstream(buffer) >> id) ) {
				try {
					Produtor *p = procuraProdutor(id);
					menu_listarUvasProdutor(id);
					cin.get();
					cin.clear();
				} catch(Erro) {
					stringCenter("Erro: Insira um id valido.");
					continue;
				}
			} else
				stringCenter("Erro: Insira um id valido.");

		}
		else if(op == 'D')
		{
			stringCenter("Digite a Casta: ",false);
			getline(cin,nome);
			menu_listarUvasCasta(nome);
			cin.get();
			cin.clear();
		}
		else if(op == 'E')
		{
			stringCenter("Digite a Especie: ",false);
			getline(cin,nome);
			menu_listarUvasEspecie(nome);
			cin.get();
			cin.clear();
		}
		else if(op == 'F')
		{
			stringCenter("Digite a Cor: ",false);
			getline(cin,nome);
			menu_listarUvasCor(nome);
			cin.get();
			cin.clear();
		}
		else if(op == 'G')
		{
			stringCenter("Digite o Aroma: ",false);
			getline(cin,nome);
			menu_listarUvasAroma(nome);
			cin.get();
			cin.clear();
		}
		else if(op == 'H')
		{
			stringCenter("Digite o Regiao: ",false);
			getline(cin,nome);
			menu_listarUvasRegiao(nome);
			cin.get();
			cin.clear();
		}
		else if(op == 'H')
		{
			double m,n;
			stringCenter("Preco1: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> m )  
			{ //Converte string para numero
				stringCenter("Preco2: ", false);
				getline(cin, buffer2);
				if ( istringstream(buffer2) >> n )
				{
					menu_listarUvasEntrePreco(m,n);
				}
				else
				{
					erro= "Erro Preco invalido";
				}
			} else {
				erro = "Erro: Preco invalido.";
			}
			cin.get();
			cin.clear();

		} else if(op == '^' || op == 'M') {
			return;
		} else {
			erro = "Erro: Opcao nao identificada.";
		}

		//Perguntar todos, entre ids, produtor, casta, especie, regiao, cor, aroma, preço

	} while(true);
}
void Cooperativa::menu_listarEmpresa()
{
	char op;
	int n1,n2;
	string nome;
	string erro;
	string buffer,buffer2;
	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar Empresa de Transporte =");
		cout << endl;

		stringCenter("A - Todos                ");
		stringCenter("B - Entre codigos        ");
		stringCenter("C - Por Nome             ");
		stringCenter("D - Capacidade Superior a");

		cout << endl;
		stringCenter("^ - Voltar Atras");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
		{
			menu_listarEmpresaTodos();
			cin.get();
			cin.clear();
		}
		else if(op == 'B')
		{
			stringCenter("ID1: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				stringCenter("ID2: ", false);
				getline(cin, buffer2);
				if ( istringstream(buffer2) >> n2 )
				{
					menu_listarEmpresaEntreId(n1,n2);
				}
				else
				{
					erro= "Erro Id invalido";
				}
			} else {
				erro = "Erro: ID invalido.";
			}
			cin.get();
			cin.clear();
		}
		else if(op == 'C')
		{
			stringCenter("Digite o Nome: ",false);
			getline(cin,nome);
			menu_listarEmpresaNome(nome);
			cin.get();
			cin.clear();
		} else if(op == 'D')
		{
			stringCenter("Capacidade: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				menu_listarEmpresaCapacidade(n1);
			} else {
				erro = "Erro: ID invalido.";
			}
			cin.get();
			cin.clear();
		}


		else if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";


	} while(true);
}
void Cooperativa::menu_listarViatura()
{
	char op;
	int n1,n2;
	string nome;
	string erro;
	string buffer,buffer2;
	do {
		system("cls");

		menu_cabecalho(erro);

		stringCenter("= Listar Viatura =");
		cout << endl;

		stringCenter("A - Todos                ");
		stringCenter("B - Por Empresa          ");
		stringCenter("C - Capacidade Superior a");

		cout << endl;
		stringCenter("^ - Voltar Atras");
		cout << endl;
		stringCenter("Opcao: ", false);

		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == 'A')
		{
			menu_listarViaturaTodos();
			cin.get();
			cin.clear();
		}
		else if(op == 'B')
		{
			stringCenter("Id da Empresa: ",false);
			getline(cin, buffer);
			if ( istringstream(buffer) >> n1 )  
			{ //Converte string para numero
				menu_listarViaturaEmpresa(n1);
			} else {
				erro = "Erro: ID invalido.";
			}
			cin.get();
			cin.clear();
		} else if(op == 'C')
		{
			stringCenter("Capacidade: ", false);
			getline(cin, buffer2);
			if ( istringstream(buffer2) >> n1 )  
			{ //Converte string para numero
				menu_listarViaturaCapacidade(n1);
			} else {
				erro = "Erro: ID invalido.";
			}
			cin.get();
			cin.clear();
		}


		else if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";


	} while(true);
}
void Cooperativa::menu_listarinativos()
{
	char op;
	string erro;
	string buffer,buffer2;
	do {
		system("cls");
		menu_cabecalho(erro);
		stringCenter("= Listar Clientes Inativos =");
		cout << endl;
		listarinativos();
		stringCenter("^ - Voltar Atras");
		cout << endl;
		stringCenter("Opcao: ", false);
		getline(cin, buffer);
		if(buffer.size() == 1)
			op = toupper(buffer[0]);
		else
			op = ' ';

		if(op == '^' || op == 'M')
			return;
		else 
			erro = "Erro: Opcao nao identificada.";


	} while(true);
}


void Cooperativa::menu_listarClientesTodos()
{
	for(unsigned int i=0;i<clientes.size();i++)
		cout << (*clientes[i]) << endl;

	cout<<endl;
}
void Cooperativa::menu_listarClientesEntreId(int n1,int n2)
{

	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}
	for(unsigned int i=0;i<clientes.size();i++)
	{
		if(clientes[i]->getId()>=n1 && clientes[i]->getId()<=n2)
			cout << (*clientes[i]) << endl;
	}
	cout<<endl;


}
void Cooperativa::menu_listarClientesNome(string nome)
{
	for(unsigned int i=0;i<clientes.size();i++)
	{
		if(clientes[i]->getNome()==nome)
			cout << (*clientes[i]) << endl;
	}
	cout<<endl;

}

void Cooperativa::menu_listarProdutosTodos()
{
	BSTItrIn<ProdutoPtr> it(produtos);

	while(!it.isAtEnd())
	{
		cout << *(it.retrieve().getPtr()) << endl << endl;

		it.advance();
	}
	cout<<endl;
}
void Cooperativa::menu_listarProdutosEntreId(int n1,int n2)
{

	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}

	BSTItrIn<ProdutoPtr> it(produtos);

	while(!it.isAtEnd())
	{
		if(it.retrieve().getPtr()->getId()>=n1 &&  it.retrieve().getPtr()->getId()<=n2)
		{
			cout << *(it.retrieve().getPtr()) << endl << endl;
		}

		it.advance();
	}
	cout<<endl;


}
void Cooperativa::menu_listarProdutosNome(string nome)
{
	BSTItrIn<ProdutoPtr> it(produtos);
	
	while(!it.isAtEnd())
	{
		if(it.retrieve().getPtr()->getNome()==nome)
		{
			cout << *(it.retrieve().getPtr()) << endl << endl;
		}

		it.advance();
	}
	cout<<endl;

}
void Cooperativa::menu_listarProdutosUvas(vector<Uva*> uvasprocura)
{
	BSTItrIn<ProdutoPtr> it(produtos);

	while(!it.isAtEnd())
	{
		int z = uvasprocura.size();

		for(int l=0;l<uvasprocura.size();l++)
		{
			for (int j=0;j<it.retrieve().getPtr()->getUvasSize();j++)
			{
				if(uvasprocura[l]->getId()==it.retrieve().getPtr()->getUvaId(j))
				{
					z--;
					break;
				}
			}
		}

		if(z != uvasprocura.size())
		{
			cout << *(it.retrieve().getPtr()) << endl << endl;
		}

		it.advance();
	}
	cout<<endl;
}
void Cooperativa::menu_listarProdutosProdutor(int n)
{
	BSTItrIn<ProdutoPtr> it(produtos);

	while(!it.isAtEnd())
	{
		for(unsigned int j=0;j<produtores.size();j++)
		{
			if(produtores[j]->getId()==n)
			{
				for(unsigned int l=0;l<produtores[j]->getProdSize();l++)
				{
					if(produtores[j]->getProdId(l)==it.retrieve().getPtr()->getId())
					{
						cout << *(it.retrieve().getPtr()) << endl << endl;
					}
				}
			}
		}

		it.advance();
	}
	cout<<endl;

}
void Cooperativa::menu_listarProdutosTipo(string nome)
{
	BSTItrIn<ProdutoPtr> it(produtos);

	bool jaEncontrou = false;

	while(!it.isAtEnd())
	{
		
		if(it.retrieve().getPtr()->getTipo()==nome)
		{
			jaEncontrou = true;
			cout << *(it.retrieve().getPtr()) << endl << endl;
		}

		if ( jaEncontrou == true && it.retrieve().getPtr()->getTipo()!=nome )
			break;
	}
	cout<<endl;

}

void Cooperativa::menu_listarUvasTodos()
{
	for(unsigned int i = 0; i < uvas.size();i++)
		cout << (*uvas[i]) << endl;
	cout<<endl;
}
void Cooperativa::menu_listarUvasEntreId(int n1,int n2)
{
	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}
	int i=0;
	for(i=0;i<uvas.size();i++)
	{
		if(uvas[i]->getId()>=n1 && uvas[i]->getId()<=n2)
			cout << (*uvas[i]) << endl;
	}
	cout<<endl;

}
void Cooperativa::menu_listarUvasProdutor(int id)
{
	for(unsigned int j = 0; j < produtores.size(); j++)
	{
		if(produtores[j]->getId()==id)
		{
			vector<Uva *> tempUvas = produtores[j]->getUvas();
			for(unsigned int i = 0; i < tempUvas.size();i++)
				cout << (*tempUvas[i]) << endl;
		}
		break;
	}
	cout<<endl;
}
void Cooperativa::menu_listarUvasCasta(string nome)
{

	int i=0;
	for(i=0;i<uvas.size();i++)
	{
		if(uvas[i]->getCasta()==nome)
			cout << (*uvas[i]) << endl;
	}
	cout<<endl;
}
void Cooperativa::menu_listarUvasEspecie(string nome)
{
	int i=0;
	for(i=0;i<uvas.size();i++)
	{
		if(uvas[i]->getEspecie()==nome)
			cout << (*uvas[i]) << endl;
	}
	cout<<endl;
}
void Cooperativa::menu_listarUvasRegiao(string nome)
{
	int i=0;
	for(i=0;i<uvas.size();i++)
	{
		if(uvas[i]->getRegiao()==nome)
			cout << (*uvas[i]) << endl;
	}
	cout<<endl;
}
void Cooperativa::menu_listarUvasCor(string nome)
{
	int i=0;
	for(i=0;i<uvas.size();i++)
	{
		if(uvas[i]->getCor()==nome)
			cout << (*uvas[i]) << endl;
	}
	cout<<endl;
}
void Cooperativa::menu_listarUvasAroma(string nome)
{
	int i=0;
	for(i=0;i<uvas.size();i++)
	{
		if(uvas[i]->getAroma()==nome)
			cout << (*uvas[i]) << endl;
	}
	cout<<endl;
}
void Cooperativa::menu_listarUvasEntrePreco(double n1,double n2)
{
	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}
	int i=0;
	for(i=0;i<uvas.size();i++)
	{
		if(uvas[i]->getPreco()>=n1 && uvas[i]->getPreco()<=n2)
			cout << (*uvas[i]) << endl;
	}
	cout<<endl;

}

void Cooperativa::menu_listarProdutorTodos()
{
	for(int i=0;i<produtores.size();i++)
		cout << (*produtores[i]) << endl;
	cout<<endl;
}
void Cooperativa::menu_listarProdutorEntreId(int n1,int n2)
{
	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}
	for(int i=0;i<produtores.size();i++)
	{
		if(produtores[i]->getId()>=n1 && produtores[i]->getId()<=n2)
			cout << (*produtores[i]) << endl;
	}
	cout<<endl;
}
void Cooperativa::menu_listarProdutorNome(string nome)
{
	for(int i=0;i<produtores.size();i++)
	{
		if(produtores[i]->getNome()==nome)
			cout << (*produtores[i]) << endl;
	}
	cout<<endl;
}
void Cooperativa::menu_listarProdutorUvas(vector<Uva*> uvasprocura)
{

	for(unsigned int i=0;i<produtores.size();i++)
	{
		int z=uvasprocura.size();

		for(int l=0;l<uvasprocura.size();l++)
		{
			for (int j=0;j<produtores[i]->getUvasSize();j++)
			{
				if(uvasprocura[l]->getId()==produtores[i]->getUvaId(j))
				{
					z--;
					break;
				}
			}
		}
		if(z==0)
		{
			cout << (*produtores[i]) << endl;
		}

	}
	cout<<endl;
}
void Cooperativa::menu_listarProdutorProdutos(vector<Produto*> produtosprocura)
{

	for(unsigned int i=0;i<produtores.size();i++)
	{
		int z=produtosprocura.size();

		for(int l=0;l<produtosprocura.size();l++)
		{
			for (int j=0;j<produtores[i]->getProdSize();j++)
			{
				if(produtosprocura[l]->getId()==produtores[i]->getProdId(j))
				{
					z--;
					break;
				}
			}
		}
		if(z==0)
		{
			cout << (*produtores[i]) << endl;
		}

	}
	cout<<endl;
}

void Cooperativa::menu_listarTransacaoEntreId(int n1,int n2)
{
	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}
	for(int i=0;i<transacoes.size();i++)
	{
		if(transacoes[i]->getId()>=n1 && transacoes[i]->getId()<=n2)
		{
			stringstream ss,ss1;
			time_t t=transacoes[i]->getData();
			struct tm *timeinfo = localtime(&t);

			ss<<"Id : "<<transacoes[i]->getId()<<" | Data : "<< (timeinfo->tm_year+1900) <<"/"<< (timeinfo->tm_mon+1) <<"/"<<timeinfo->tm_mday<<" | Comprador : "<<transacoes[i]->getClienteId()<<" | Vendedor : "<<transacoes[i]->getVendedorId();
			stringCenter(ss.str());
			ss1<<"Produtos: ";
			map<Produto *, int> produtos2=transacoes[i]->getProdutos();
			for(map<Produto *, int>::iterator j=produtos2.begin();j!=produtos2.end();j++)
			{
				ss1<<" Id : "<<(*j).first->getId()<<" Quantidade: "<<(*j).second;
			}
			stringCenter(ss1.str());
			stringCenter("Montante Total: "+DoubleToString(transacoes[i]->getMontante()));
		}

	}
}
void Cooperativa::menu_listarTransacaoTodos()
{
	for(int i=0;i<transacoes.size();i++)
		cout << (*transacoes[i]) << endl;
}
void Cooperativa::menu_listarTransacaoComprador(int n1)
{


	for(int i=0;i<transacoes.size();i++)
	{
		if(transacoes[i]->getClienteId()==n1)
			cout << (*transacoes[i]) << endl;

	}

}
void Cooperativa::menu_listarTransacaoVendedor(int n1)
{
	for(int i=0;i<transacoes.size();i++)
	{
		if(transacoes[i]->getClienteId()==n1)
			cout << (*transacoes[i]) << endl;
	}
}
void Cooperativa::menu_listarTransacaoEntreData(std::time_t n1,std::time_t n2)
{
	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}
	for(int i=0;i<transacoes.size();i++)
	{
		if(transacoes[i]->getData()>=n1 && transacoes[i]->getData()<=n2)
			cout << (*transacoes[i]) << endl;
	}
}
void Cooperativa::menu_listarTransacaoProdutos(vector<Produto *> produtosprocura)
{
	for(unsigned int i=0;i<transacoes.size();i++)
	{
		int z=produtosprocura.size();

		for(int l=0;l<produtosprocura.size();l++)
		{
			map<Produto *, int> produtos2=transacoes[i]->getProdutos();
			for(map<Produto *, int>::iterator j=produtos2.begin();j!=produtos2.end();j++)
			{
				if(produtosprocura[l]->getId()==(*j).first->getId())
				{
					z--;
					break;
				}
			}
		}
		if(z==0)
			cout << (*transacoes[i]) << endl;

	}
	cout<<endl;

}

void Cooperativa::menu_listarEmpresaTodos()
{
	priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

	while(!tempEmpresasTransporte.empty())
	{
		cout << tempEmpresasTransporte.top() << endl;
		tempEmpresasTransporte.pop();
	}
	cout << endl;
}
void Cooperativa::menu_listarEmpresaEntreId(int n1, int n2)
{
	int temp;
	if(n1>n2)
	{
		temp=n1;
		n1=n2;
		n2=temp;
	}

	priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

	while(!tempEmpresasTransporte.empty())
	{
		if(tempEmpresasTransporte.top().getId() > n1 && tempEmpresasTransporte.top().getId() < n2)
			cout << tempEmpresasTransporte.top() << endl;
		tempEmpresasTransporte.pop();
	}
	cout << endl;
}
void Cooperativa::menu_listarEmpresaNome(string nome)
{
	priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

	while(!tempEmpresasTransporte.empty())
	{
		if(tempEmpresasTransporte.top().getNome() == nome)
			cout << tempEmpresasTransporte.top() << endl;
		tempEmpresasTransporte.pop();
	}
	cout << endl;
}
void Cooperativa::menu_listarEmpresaCapacidade(double n)
{
	priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

	while(!tempEmpresasTransporte.empty())
	{
		if(tempEmpresasTransporte.top().getCapacidade() >= n)
			cout << tempEmpresasTransporte.top() << endl;
		tempEmpresasTransporte.pop();
	}
	cout << endl;
}

void Cooperativa::menu_listarViaturaTodos()
{
	priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

	while(!tempEmpresasTransporte.empty())
	{
		list<Viatura> tempViaturas = tempEmpresasTransporte.top().getViaturas();
		list<Viatura>::iterator it;
		for(it = tempViaturas.begin(); it != tempViaturas.end(); it++)
			cout << (*it) << endl;
		tempEmpresasTransporte.pop();
	}
	cout << endl;
}
void Cooperativa::menu_listarViaturaEmpresa(int id)
{
	priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

	while(!tempEmpresasTransporte.empty())
	{
		if(tempEmpresasTransporte.top().getId() == id)
		{
			list<Viatura> tempViaturas = tempEmpresasTransporte.top().getViaturas();
			list<Viatura>::iterator it;
			for(it = tempViaturas.begin(); it != tempViaturas.end(); it++)
				cout << (*it) << endl;
		}		
		tempEmpresasTransporte.pop();
	}
	cout << endl;
}
void Cooperativa::menu_listarViaturaCapacidade(double n)
{
	priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

	while(!tempEmpresasTransporte.empty())
	{
		list<Viatura> tempViaturas = tempEmpresasTransporte.top().getViaturas();
		list<Viatura>::iterator it;
		for(it = tempViaturas.begin(); it != tempViaturas.end(); it++)
		{
			if( (*it).getCapacidade() >= n)
				cout << (*it) << endl;
	
		}
		tempEmpresasTransporte.pop();
	}
	cout << endl;
}

