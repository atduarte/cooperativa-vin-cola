#include "cooperativa.h"

Cooperativa::Cooperativa(string tempNome): produtos(ProdutoPtr(0))
{
	nome=tempNome;
}

Cooperativa::Cooperativa() : produtos(ProdutoPtr(0)) {}

/** \brief Adiciona um dado Produtor à Cooperativa
* 
*   Adiciona o apontador ao vector produtores.
*   Retorna verdadeiro em caso de sucesso.
*/
bool Cooperativa::addProdutor(Produtor *tempProdutor) 
{
	produtores.push_back(tempProdutor);
	return true;
}

/** \brief Adiciona um dado Cliente à Cooperativa
* 
*   Adiciona o apontador ao vector clientes.
*   Retorna verdadeiro em caso de sucesso.
*/
bool Cooperativa::addCliente(Cliente *tempCliente) 
{
	clientes.push_back(tempCliente);
	return true;
}

/** \brief Adiciona uma dada Transação à Cooperativa
* 
*   Adiciona o apontador ao vector transacoes.
*   Retorna verdadeiro em caso de sucesso.
*/
bool Cooperativa::addTransacao(Transacao *tempTransacao) 
{
	transacoes.push_back(tempTransacao);
	return true;
}

/** \brief Adiciona um dado Produto à Cooperativa
* 
*   Adiciona o apontador ao vector produtos.
*   Retorna verdadeiro em caso de sucesso.
*/
bool Cooperativa::addProduto(Produto *tempProduto) 
{
	ProdutoPtr p (tempProduto);
	produtos.insert(p);
	return true;
}

/** \brief Adiciona uma dada Uva à Cooperativa
* 
*   Adiciona o apontador ao vector uvas.
*   Retorna verdadeiro em caso de sucesso.
*/
bool Cooperativa::addUva(Uva *tempUva) 
{
	uvas.push_back(tempUva);
	return true;
}

/** \brief Adiciona uma dada Empresa de transporte à Cooperativa
* 
*   Adiciona à fila de prioridade empresasTransporte
*   Retorna verdadeiro em caso de sucesso.
*/
bool Cooperativa::addEmpresaTransporte(EmpresaTransporte &tempEmpresa)
{
	empresasTransporte.push(tempEmpresa);
	return true;
}



/** \brief Elimina dados associados ao Cliente com o id passado como argumento
* 
*   Elimina do vector cliente e as transações, no vector transacoes, feitas por ele
*   Retorna verdadeiro, caso encontre o Cliente. Retorna falso, se não encontrar.
*/
bool Cooperativa::eliminaCliente(int id)
{
	bool found = false;
	for(unsigned int i = 0; i < clientes.size(); i++)
	{
		if( clientes[i]->getId() == id)
		{
			found = true;
			eliminainativo(*clientes[i]);
			clientes.erase(clientes.begin() + i);
			break;
		}
	}

	for(unsigned int j = 0; j < transacoes.size(); j++)
	{
		if( transacoes[j]->getClienteId() == id)
		{
			transacoes.erase(transacoes.begin() + j );
			j--;
		}
	}

	return found;
}

/** \brief Elimina dados associados ao Produtor com o id passado como argumento
* 
*   Elimina do vector produtores e as Transações, no vector transacoes, feitas por ele
*   Retorna verdadeiro, caso encontre o Produtor. Retorna falso, se não encontrar.
*/
bool Cooperativa::eliminaProdutor(int id)
{
	bool found = false;
	for(int j=0;j<produtores.size();j++) {
		if(produtores[j]->getId()==id)
		{
			found = true;
			produtores.erase(produtores.begin()+j);
			break;
		}
	}

	for(int i=0;i<transacoes.size();i++)
	{
		if(transacoes[i]->getVendedorId() == id)
		{
			transacoes.erase(transacoes.begin()+i);
			i--;
		}
	}

	return found;
}

/** \brief Elimina dados associados à Transacao com o id passado como argumento
* 
*   Elimina do vector transacoes a Transacao com esse id
*   Retorna verdadeiro, caso encontre o Transacao. Retorna falso, se não encontrar.
*/
bool Cooperativa::eliminaTransacao(int id)
{
	bool found = false;
	for(int i=0; i<transacoes.size(); i++)
	{
		if(transacoes[i]->getId() == id)
		{
			found = true;
			transacoes.erase(transacoes.begin()+i);
			i--;
		}
	}
	return found;
}

/** \brief Elimina dados associados ao Produto com o id passado como argumento
* 
*   Elimina do vector produtos o Produto com esse id, tal como das transacoes.
*   Retorna verdadeiro, caso encontre o Produto. Retorna falso, se não encontrar.
*/
bool Cooperativa::eliminaProduto(int id)
{
	bool found = false;
	BSTItrIn<ProdutoPtr> it (produtos);

	while(!it.isAtEnd())
	{
		if(it.retrieve().getPtr()->getId() == id)
		{
			produtos.remove(it.retrieve());
			break;
		}

		it.advance();

	}

	for(unsigned int j = 0; j < produtores.size(); j++)
		produtores[j]->eliminarProdId(id);

	for(unsigned int j = 0; j < transacoes.size(); j++) {
		transacoes[j]->eliminarProdId(id);
		if(transacoes[j]->getProdutosSize() == 0) {
			transacoes.erase( transacoes.begin() + j);
			j--;
		}
	}

	return found;
}

/** \brief Elimina dados associados ao Uva com o id passado como argumento
* 
*   Elimina do vector uvas, da Cooperativa e dos Produtores, a Uva com esse id, tal como dos produtos aqueles que têm esta Uva na sua composição.
*   Retorna verdadeiro, caso encontre o Uva. Retorna falso, se não encontrar.
*/
bool Cooperativa::eliminaUva(int id)
{
	bool found = false;
	for(unsigned int i=0; i < uvas.size(); i++) {
		if(uvas[i]->getId() == id) {
			found = true;
			uvas.erase(uvas.begin() + i);
			break;
		}
	}

	for(unsigned int i = 0; i < produtores.size(); i++)
		produtores[i]->eliminarUvaId(id);

	BSTItrIn<ProdutoPtr> it (produtos); 

	while(!it.isAtEnd())
	{
		if(it.retrieve().getPtr()->procuraUva(id)) 
		{
			eliminaProduto( it.retrieve().getPtr()->getId() );
		}

		it.advance();
	}

	return found;
}

/** \brief Elimina dados à Viatura com o id passado como argumento
* 
*   Elimina da lista na Empresa de Transporte, a Viatura com esse id.
*   Retorna verdadeiro, caso encontre o Viatura. Retorna falso, se não encontrar.
*/
bool Cooperativa::eliminaViatura(int id)
{
	bool found = false;

	stack<EmpresaTransporte> tempEmpresasTransporte;

	while(!empresasTransporte.empty())
	{
		if(empresasTransporte.top().removeViatura(id))
		{
			found = true;
			break;
		}

		tempEmpresasTransporte.push( empresasTransporte.top() );
		empresasTransporte.pop();
	}

	while(!tempEmpresasTransporte.empty())
	{
		empresasTransporte.push( tempEmpresasTransporte.top() );
		tempEmpresasTransporte.pop();
	}

	return found;
}

/** \brief Elimina dados associados à Empresa de Transporte com o id passado como argumento
* 
*   Elimina da fila de prioridade empresasTransporte, da Cooperativa, a Empresa de Transporte com esse id.
*   Retorna verdadeiro, caso encontre o Empresa. Retorna falso, se não encontrar.
*/
bool Cooperativa::eliminaEmpresaTransporte(int id)
{
	bool found = false;

	stack<EmpresaTransporte> tempEmpresasTransporte;

	while(!empresasTransporte.empty())
	{
		if(empresasTransporte.top().getId() == id)
		{
			found = true;
			empresasTransporte.pop();
			break;
		}

		tempEmpresasTransporte.push( empresasTransporte.top() );
		empresasTransporte.pop();
	}

	while(!tempEmpresasTransporte.empty())
	{
		empresasTransporte.push( tempEmpresasTransporte.top() );
		tempEmpresasTransporte.pop();
	}

	for(int i=0; i<transacoes.size(); i++)
	{
		if(transacoes[i]->getEmpresaTransporteId == id)
		{
			transacoes.erase(transacoes.begin()+i);
			i--;
		}
	}

	return found;
}

/** \brief Elimina objectos chamando a funções elimina...() de acordo com o char op
*
*   'C' - eliminaCliente(id);  'R' - eliminaProdutor(id);  'T' - eliminaTransacao(id); 'P' - eliminaProduto(id); 'U' - eliminaUva(id); 
*   Retorna verdadeiro, a função respectiva devolva verdadeiro. Retorna falso, em caso contrário ou não reconhecendo o char op.
*/
bool Cooperativa::elimina(char op,int id)
{
	Cliente a(id); 
	switch(op)
	{
	case 'C': return eliminaCliente(id);

	case 'E': return eliminaEmpresaTransporte(id);

	case 'I': return eliminainativo(a);

	case 'R': return eliminaProdutor(id);

	case 'T': return eliminaTransacao(id);

	case 'P': return eliminaProduto(id);

	case 'U': return eliminaUva(id);

	case 'V': return eliminaViatura(id);

	default: return false;
	}
}



Uva* Cooperativa::procuraUva(int Id)
{
	for(unsigned int i = 0; i < uvas.size(); i++)
		if(uvas[i]->getId() == Id) 
			return uvas[i];

	throw Erro();
}

vector <Uva*> Cooperativa::procuraUvas(string ids, string needle)
{
	vector <int> tempIds = explode(ids, needle);

	vector <Uva*> resultado;
	Uva* u;

	for(unsigned int i = 0; i < tempIds.size(); i++) {
		try {
			u = procuraUva( tempIds[i] );
		} catch(Erro) {
			continue;
		}

		resultado.push_back(u); 
	} 

	return resultado;

}

Produto* Cooperativa::procuraProduto(int Id)
{
	BSTItrIn<ProdutoPtr> it (produtos);

	while(!it.isAtEnd())
	{
		if(it.retrieve().getPtr()->getId() == Id)
		{
			return it.retrieve().getPtr();
		}

		it.advance();

	}

	throw Erro();
}

vector <Produto*> Cooperativa::procuraProdutos(string ids, string needle)
{
	vector <int> tempIds = explode(ids, needle);

	vector <Produto*> resultado;
	Produto* u;

	for(unsigned int i = 0; i < tempIds.size(); i++) {
		try {
			u = procuraProduto( tempIds[i] );
		} catch(Erro) {
			continue;
		}

		resultado.push_back(u); 
	} 

	return resultado;
}

Produtor* Cooperativa::procuraProdutor(int Id)
{
	for(unsigned int i = 0; i < produtores.size(); i++)
		if(produtores[i]->getId() == Id) 
			return produtores[i];

	throw Erro();
}

Cliente* Cooperativa::procuraCliente(int Id)
{
	for(unsigned int i = 0; i < clientes.size(); i++)
		if(clientes[i]->getId() == Id) 
			return clientes[i];

	throw Erro();
}

Transacao* Cooperativa::procuraTransacao(int Id)
{
	for(unsigned int i = 0; i < transacoes.size(); i++)
		if(transacoes[i]->getId() == Id) 
			return transacoes[i];

	throw Erro();
}

EmpresaTransporte Cooperativa::procuraEmpresaTransporte(int Id)
{
	EmpresaTransporte result;
	bool found = false;

	stack<EmpresaTransporte> tempEmpresasTransporte;

	while(!empresasTransporte.empty())
	{
		if(empresasTransporte.top().getId() == Id)
		{
			found = true;
			result = empresasTransporte.top();
			break;
		}

		tempEmpresasTransporte.push( empresasTransporte.top() );
		empresasTransporte.pop();
	}

	while(!tempEmpresasTransporte.empty())
	{
		empresasTransporte.push( tempEmpresasTransporte.top() );
		tempEmpresasTransporte.pop();
	}

	if(!found)
		throw Erro();
	else
		return result;
}


int Cooperativa::arranjaTransporte(double capacidade)
{
	int result;
	bool found = false;

	stack<EmpresaTransporte> tempEmpresasTransporte;

	while(!empresasTransporte.empty())
	{
		if(empresasTransporte.top().getCapacidade() >= capacidade)
		{
			found = true;
			result = empresasTransporte.top().getId();
			break;
		}

		tempEmpresasTransporte.push( empresasTransporte.top() );
		empresasTransporte.pop();
	}

	while(!tempEmpresasTransporte.empty())
	{
		empresasTransporte.push( tempEmpresasTransporte.top() );
		tempEmpresasTransporte.pop();
	}

	if(!found)
		throw Erro();
	else
		return result;
}

bool Cooperativa::carregaFicheiro()
{

	ifstream ficheiro;
	ficheiro.open("cooperativa.txt");

	if (ficheiro.is_open())
	{
		string linha;
		int estado = 0; // 1 - Nome; 2 - Clientes; 3 - Uvas; 4 - Produtos; 5 - Produtores; 6 - Transaccoes; 7 - Empresas de Transporte; 8 - Viaturas

		while(!ficheiro.eof())
		{
			getline(ficheiro, linha);

			if(linha != "")
			{
				if(estado == 0)
					estado = converteEmEstado(linha);
				else
					leLinha(linha, estado);
			} else {
				estado = 0;
			}
		}

		ficheiro.close();
		actualizainativos();
		return true;
	} else {
		return false;
	}
}

/*Funcao de escrita no ficheiro. Começa por renomear o ficheiro antigo e abrir um novo ficheiro. Efectua a escrita no formato pretendido
e no final apaga o antigo ficheiro deixando apenas o que acabou de escrever*/
string Cooperativa::escreveFicheiro() const
{
	rename("cooperativa.txt", "cooperativa_temp.txt");

	ofstream ficheiro;
	ficheiro.open("cooperativa.txt");

	if (ficheiro.is_open())
	{
		ficheiro << endl 
			<< "*** Nome" << endl
			<< nome << endl << endl
			<< "*** Clientes" << endl;

		for (vector<Cliente *>::const_iterator it = clientes.begin(); it < clientes.end() ; it++)
			ficheiro << (*it)->getId() << " |" << (*it)->getNome() << "| |" << (*it)->getContacto() << "|" << endl;

		ficheiro << endl
			<< "*** Uvas" << endl;

		for (vector<Uva *>::const_iterator it = uvas.begin(); it < uvas.end() ; it++)
			ficheiro << (*it)->getId() 
			<< " |" << (*it)->getCasta()	<< "|"
			<< " |" << (*it)->getEspecie()	<< "|"
			<< " |" << (*it)->getRegiao()	<< "|"
			<< " |" << (*it)->getCor()		<< "|"
			<< " |" << (*it)->getAroma()	<< "|"
			<< " |" << (*it)->getPreco()	<< "|" << endl;

		ficheiro << endl
			<< "*** Produtos" << endl;

		BSTItrIn<ProdutoPtr> it (produtos);

		while(!it.isAtEnd())
		{
			ficheiro << it.retrieve().getPtr()->getId() << " {";

			int i = 0;
			map<Uva *, float> temp_uvas = it.retrieve().getPtr()->getUvas();
			for(map<Uva * , float>::iterator itr = temp_uvas.begin(); itr != temp_uvas.end(); itr++ )
			{
				ficheiro << "[" << (*itr).first->getId() << " " << (*itr).second << "]";

				if (i - (int)temp_uvas.size() != -1)
					ficheiro << ",";		

				i++;
			}
			ficheiro << "} ";

			ficheiro << "|" << it.retrieve().getPtr()->getNome() << "| ";

			map<string, string> temp_atr = it.retrieve().getPtr()->getAtributos();

			ficheiro << "|" << temp_atr["tipo"] << "| ";

			if(temp_atr["tipo"] == "Vinho" || temp_atr["tipo"] == "VinhoDoPorto" || temp_atr["tipo"] == "Derivado")
				ficheiro << "|" << temp_atr["subTipo"] << "| ";

			if(temp_atr["tipo"] == "Vinho" || temp_atr["tipo"] == "VinhoDoPorto" || temp_atr["tipo"] == "Espumante" ) {
				ficheiro << "|" << temp_atr["anoColheita"] << "| ";
				ficheiro << "|" << temp_atr["volumeAlcoolico"] << "| ";
			}

			if(temp_atr["tipo"] == "VinhoDoPorto")
				ficheiro << "|" << temp_atr["tempoArmazenamento"] << "|";

			if(temp_atr["tipo"] == "Espumante" ) {
				ficheiro << "|" << temp_atr["volumeCo2"] << "| ";
				ficheiro << "|" << temp_atr["qualidadeCo2"] << "|";
			}

			if(temp_atr["tipo"] == "Derivado")
				ficheiro << "|" << temp_atr["preco"] << "| ";

			ficheiro << endl;

			it.advance();
		}



		ficheiro << endl
			<< "*** Produtores" << endl;

		for (vector<Produtor *>::const_iterator it = produtores.begin(); it < produtores.end() ; it++)
		{ 
			//ID + Nome
			ficheiro << (*it)->getId() << " |" << (*it)->getNome() << "| ";

			//Produtos
			ficheiro << "{";
			for (unsigned int i = 0; i < (*it)->getProdSize(); i++)
			{ 
				ficheiro << (*it)->getProdId(i);
				if (i - (*it)->getProdSize() != -1)
					ficheiro << " ";
			}
			ficheiro << "} ";

			//Uvas
			ficheiro << "{";
			for (unsigned int i = 0; i < (*it)->getUvasSize(); i++)
			{ 
				ficheiro << (*it)->getUvaId(i);
				if (i - (*it)->getUvasSize() != -1)
					ficheiro << " ";
			}
			ficheiro << "}" << endl;

		}

		ficheiro << endl;
		ficheiro << "*** Empresas de Transporte" << endl;

		priority_queue<EmpresaTransporte> tempEmpresasTransporte = empresasTransporte;

		while(!tempEmpresasTransporte.empty())
		{
			ficheiro << tempEmpresasTransporte.top().getId() << " |" << tempEmpresasTransporte.top().getNome() << "| |" 
				<< tempEmpresasTransporte.top().getDistancia() << "| {";

			list <Viatura> tempViaturas = tempEmpresasTransporte.top().getViaturas();
			list <Viatura>::iterator it;

			for(it = tempViaturas.begin(); it != tempViaturas.end(); it++)
			{
				ficheiro << "[" << (*it).getId() << " " << (*it).getCapacidade() << "]";

				list <Viatura>::iterator it_ = it;
				it_++;

				if(it_ != tempViaturas.end())
					ficheiro << ",";
			}

			ficheiro << "}" << endl;

			tempEmpresasTransporte.pop();
		}

		ficheiro << endl;
		ficheiro << "*** Transacoes" << endl;

		for (vector<Transacao *>::const_iterator it = transacoes.begin(); it < transacoes.end() ; it++)
		{
			//ID + Data + ClienteId + VendedorId
			ficheiro << (*it)->getId() << " " << (*it)->getData() << " " << (*it)->getClienteId() << " " << (*it)->getVendedorId() << " ";

			//Produtos
			ficheiro << "{";
			int i = 0;
			map<Produto*, int> temp = (*it)->getProdutos();
			for(map<Produto* , int>::iterator itr = temp.begin(); itr != temp.end(); itr++ )
			{
				ficheiro << "[" << (*itr).first->getId() << " " << (*itr).second << "]";

				if (i - (int)temp.size() != -1)
					ficheiro << ",";		

				i++;
			}
			ficheiro << "} ";

			//Montante
			ficheiro << "|" << (*it)->getMontante() << "| ";

			//Empresa de Transporte
			ficheiro << "|" << (*it)->getEmpresaTransporteId() << "|" << endl;
		}


	}



	ficheiro.close();

	if( remove( "cooperativa_temp.txt" ) != 0 )
		return "Erro: Foi impossivel reescrever o ficheiro.";
	else
		return "Sucesso: Escrita bem sucedida";
}

int Cooperativa::converteEmEstado(string linha) const
{
	if(linha == "*** Nome")
		return 1;
	else if(linha == "*** Clientes")
		return 2;
	else if(linha == "*** Uvas")
		return 3;
	else if(linha == "*** Produtos")
		return 4;
	else if(linha == "*** Produtores")
		return 5;
	else if(linha == "*** Transacoes")
		return 6;
	else if(linha == "*** Empresas de Transporte")
		return 7;
	else
		return 0;
}

void Cooperativa::leLinha(string linha, int estado) 
{
	if(estado == 1) {// Nome
		nome = linha;

	} else if(estado == 2) { //Clientes

		//Separar elementos da linha
		int tempId		= stringCutId(linha);
		string tempNome = stringCutConteudo(linha, "|", "|");
		string tempContacto = stringCutConteudo(linha, "|", "|");

		//Criar apontador e adicionar Cliente
		Cliente *c = new Cliente(tempId, tempNome, tempContacto);
		addCliente(c);

	} else if(estado == 3) { //Uvas

		//Separar elementos da linha
		int tempId			= stringCutId(linha);
		string tempCasta	= stringCutConteudo(linha, "|", "|");
		string tempEspecie	= stringCutConteudo(linha, "|", "|");
		string tempRegiao	= stringCutConteudo(linha, "|", "|");
		string tempCor		= stringCutConteudo(linha, "|", "|");
		string tempAroma	= stringCutConteudo(linha, "|", "|");
		string tempPreco	= stringCutConteudo(linha, "|", "|");

		//Criar apontador e adicionar Uva
		Uva *u = new Uva(tempId, tempCasta, tempEspecie, tempRegiao, tempCor, tempAroma, atof(tempPreco.c_str()) );
		addUva(u);

	} else if(estado == 4) { //Produtos

		//Separar elementos da linha
		int tempId							= stringCutId(linha);
		vector< vector<float> > tempUvasId	= explodeD(stringCutConteudo(linha, "{", "}"), ",", " ");
		string tempNome						= stringCutConteudo(linha, "|", "|");
		string tempTipo						= stringCutConteudo(linha, "|", "|");

		map< Uva *, float > tempUvas;
		//Corresponder IDs das uvas aos apontadores
		Uva *u;
		bool erro;
		for(unsigned int i = 0; i < tempUvasId.size(); i++) {
			erro = false;
			try {
				u = procuraUva( tempUvasId[i][0] );
			} catch(Erro) {
				cout << "Erro: Uva nao encontrada." << endl;
				erro = true;
			}
			if(!erro)
				tempUvas[u] =  tempUvasId[i][1];
		} 

		string tempSubTipo, tempQualidadeCo2;		
		int tempAnoColheita, tempTempoArmazenamento;		
		double tempVolumeAlcoolico, tempVolumeCo2;

		if(tempTipo == "Vinho" || tempTipo == "VinhoDoPorto")
			tempSubTipo			= stringCutConteudo(linha, "|", "|");

		if(tempTipo == "Vinho" || tempTipo == "VinhoDoPorto" || tempTipo == "Espumante") {
			tempAnoColheita		= atoi( stringCutConteudo(linha, "|", "|").c_str() );
			tempVolumeAlcoolico	= atof( stringCutConteudo(linha, "|", "|").c_str() );
		} 

		if(tempTipo == "Vinho") {
			Vinho *v1 = new Vinho(tempId, tempNome, tempUvas, tempSubTipo, tempAnoColheita, tempVolumeAlcoolico);
			addProduto(v1);
		}

		if(tempTipo == "VinhoDoPorto") {
			tempTempoArmazenamento	= atoi( stringCutConteudo(linha, "|", "|").c_str() );
			VinhoDoPorto *vp1 = new VinhoDoPorto(tempId, tempNome, tempUvas, tempSubTipo, tempAnoColheita, tempVolumeAlcoolico, tempTempoArmazenamento);
			addProduto(vp1);
		}

		if(tempTipo == "Espumante") {
			tempVolumeCo2		= atof( stringCutConteudo(linha, "|", "|").c_str() );
			tempQualidadeCo2	= stringCutConteudo(linha, "|", "|");

			Espumante *e1 = new Espumante(tempId, tempNome, tempUvas, tempAnoColheita, tempVolumeAlcoolico, tempVolumeCo2, tempQualidadeCo2);
			addProduto(e1);
		}

		if(tempTipo == "Derivado")  {
			string tempSubTipo	= stringCutConteudo(linha, "|", "|");
			string tempPreco		= stringCutConteudo(linha, "|", "|");
			Derivado *d1 = new Derivado(tempId, tempNome, tempUvas, tempSubTipo, atof(tempPreco.c_str()) );
			addProduto(d1);
		}


	} else if(estado == 5) { //Produtores

		//Separar elementos da linha
		int tempId					= stringCutId(linha);
		string tempNome				= stringCutConteudo(linha, "|", "|");
		vector<int> tempProdutosId	= explode(stringCutConteudo(linha, "{", "}"), " ");
		vector<int> tempUvasId		= explode(stringCutConteudo(linha, "{", "}"), " ");

		vector<Produto *> tempProdutos;
		vector<Uva *> tempUvas;

		//Corresponder IDs dos produtos aos apontadores
		Produto *p;
		bool erro;
		for(unsigned int i = 0; i < tempProdutosId.size(); i++) {
			erro = false;
			try {
				p = procuraProduto( tempProdutosId[i] );
			} catch(Erro) {
				cout << "Erro: Produto (ID:" <<  tempProdutosId[i] << ") nao encontrado." << endl;
				erro = true;
			}
			if(!erro)
				tempProdutos.push_back(p);
		}

		//Corresponder IDs das uvas aos apontadores
		Uva *u;
		for(unsigned int i = 0; i < tempUvasId.size(); i++) {
			erro = false;
			try {
				u = procuraUva( tempUvasId[i] );
			} catch(Erro) {
				cout << "Erro: Uva nao encontrada." << endl;
				erro = true;
			}
			if(!erro)
				tempUvas.push_back(u);
		} 

		//Criar apontador e adicionar Produtor
		Produtor *pr = new Produtor(tempId, tempNome, tempProdutos, tempUvas);
		addProdutor(pr);

	} else if(estado == 6) { //Transaçoes

		//Separar elementos da linha
		int tempId								= stringCutId(linha);
		std::time_t tempTimestamp				= stringCutTime(linha);
		int tempClienteId						= stringCutId(linha);
		int tempProdutorId						= stringCutId(linha);
		vector< vector<float> > tempProdutosId	= explodeD(stringCutConteudo(linha, "{", "}"), ",", " ");
		float tempMontante						= atof(stringCutConteudo(linha, "|", "|").c_str());
		int tempEmpresaTransporteId				= atoi(stringCutConteudo(linha, "|", "|").c_str());

		Cliente* tempCliente;
		Produtor* tempProdutor;	
		map<Produto *, int> tempProdutos;
		vector<Uva *> tempUvas;

		//Corresponder ID do CLIENTE ao apontador
		try {
			tempCliente = procuraCliente( tempClienteId );
		} catch(Erro) {
			cout << "Erro: Cliente nao encontrado." << endl;
			return;
		}

		//Corresponder ID do PRODUTOR ao apontador
		try {
			tempProdutor = procuraProdutor( tempProdutorId );
		} catch(Erro) {
			cout << "Erro: Produtor nao encontrado." << endl;
			return;
		}

		//Corresponder IDs dos PRODUTOS aos apontadores
		Produto *p;
		bool erro;
		for(unsigned int i = 0; i < tempProdutosId.size(); i++) {
			erro = false;
			try {
				p = procuraProduto( tempProdutosId[i][0] );
			} catch(Erro) {
				cout << "Erro: Produto (id = " <<  tempProdutosId[i][0] << ") nao encontrado." << endl;
				erro = true;
			}
			if(!erro)
				tempProdutos[p] = tempProdutosId[i][1];
		}

		//Criar apontador e adicionar Produtor
		Transacao *t = new Transacao(tempId, tempTimestamp, tempCliente, tempProdutor, tempProdutos, tempMontante, tempEmpresaTransporteId);
		addTransacao(t);
	} else if(estado == 7) { // Empresas de Transporte

		//Separar elementos da linha
		int tempId								= stringCutId(linha);
		string tempNome							= stringCutConteudo(linha, "|", "|");
		double tempDistancia					= atof(stringCutConteudo(linha, "|", "|").c_str());
		vector< vector<float> > tempViaturasS	= explodeD(stringCutConteudo(linha, "{", "}"), ",", " ");

		//Adicionar Viaturas
		list<Viatura> tempViaturas;
		for(unsigned int i = 0; i < tempViaturasS.size(); i++)
		{
			if(tempViaturasS[i].size() != 2)
				continue;

			Viatura temp((int)tempViaturasS[i][0], tempViaturasS[i][1]);
			tempViaturas.push_back( temp );
		}

		//Criar objecto e adicionar
		EmpresaTransporte et (tempId, tempNome, tempDistancia, tempViaturas);
		addEmpresaTransporte(et);
	}

	return;
}

void Cooperativa::actualizainativos()
{
	time_t t;
	t=time(NULL);
	//Segundos de um ano;
	t=t-3153600;
	for(int i=0;i<clientes.size();i++)
	{
		bool adiciona=true;
		for(int j=0;j<transacoes.size();j++)
		{
			if(clientes[i]->getId() == transacoes[i]->getClienteId() && transacoes[i]->getData() > t)
			{
				adiciona=false;
				break;
			}
		}
		if(adiciona)
		{
			clientesinativos.insert(*clientes[i]);
		}
	}
}

bool Cooperativa::eliminainativo(Cliente c1)
{
	unordered_set<Cliente, hCliente, hCliente>::iterator it = clientesinativos.find(c1);
	if(it!=clientesinativos.end())
	{
		clientesinativos.erase(it);
		return true;
	}
	return false;
}


void Cooperativa::listarinativos()
{
	unordered_set<Cliente, hCliente, hCliente>::const_iterator it;
	for(it=clientesinativos.begin();it!=clientesinativos.end();it++)
		cout<<(*it)<<endl;
}
